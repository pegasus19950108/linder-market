import React, { Component } from 'react';
import { View } from 'react-native';
import { Provider } from 'react-redux';
import SplashScreen from './containers/splashScreen';

import configureStore from './configureStore';
import RootNavigator from './rootNavigator';
const store = configureStore();

export default class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <RootNavigator/>
            </Provider>
        );
    }
}