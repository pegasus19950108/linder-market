import { applyMiddleware, createStore, compose } from 'redux';
import logger from 'redux-logger';
import devTools from 'remote-redux-devtools';
import createSagaMiddleware from 'redux-saga';

import AppReducer from '@redux/reducers';

const sagaMiddleware = createSagaMiddleware();

function configureStore(initialState = {}) {
    const middlewares = [
        sagaMiddleware,
        logger
    ];

    const enhancers = [
        applyMiddleware(...middlewares)
    ];

    const store = createStore(
        AppReducer,
        initialState,
        compose(...enhancers)
    );

    return store;
}

module.exports = configureStore;