export const img_logo = require("../assets/images/logo.png");
export const img_arrow_back = require("../assets/images/arrow-back.png");

export const img_icon_add = require("../assets/images/icon-add.png");
export const img_icon_earth = require("../assets/images/icon-earth.png");
export const img_icon_star = require("../assets/images/icon-star.png");
export const img_icon_star_filled = require("../assets/images/icon-star-filled.png");
export const img_icon_like = require("../assets/images/icon-like.png");
export const img_icon_unlike = require("../assets/images/icon-unlike.png");
export const img_icon_menu = require("../assets/images/icon-menu.png");
export const img_icon_category = require("../assets/images/icon-category.png");
export const img_icon_camera = require("../assets/images/icon-camera.png");
export const img_icon_download = require("../assets/images/icon-download.png");


export const img_icon_home = require("../assets/images/icon-home.png");
export const img_icon_home_active = require("../assets/images/icon-home-active.png");

export const img_icon_notification = require("../assets/images/icon-notification.png");
export const img_icon_notification_active = require("../assets/images/icon-notification-active.png");

export const img_icon_favorite = require("../assets/images/icon-favorite.png");
export const img_icon_favorite_active = require("../assets/images/icon-favorite-active.png");

export const img_icon_follow_up = require("../assets/images/icon-follow-up.png");
export const img_icon_follow_up_active = require("../assets/images/icon-follow-up-active.png");

export const img_icon_setting = require("../assets/images/icon-setting.png");
export const img_icon_setting_active = require("../assets/images/icon-setting-active.png");

export const img_icon_close = require('../assets/images/icon-back.png');
export const img_icon_search = require('../assets/images/icon-search.png');