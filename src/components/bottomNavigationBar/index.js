import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity ,StyleSheet } from 'react-native';

import {
    img_icon_home,
    img_icon_home_active,
    img_icon_notification,
    img_icon_notification_active,
    img_icon_follow_up,
    img_icon_follow_up_active,
    img_icon_favorite,
    img_icon_favorite_active,
    img_icon_setting,
    img_icon_setting_active,
} from '@constants';


class BottomNavigationBar extends Component {
    render(){
        let     home = img_icon_home,
                notification = img_icon_notification,
                followUp = img_icon_follow_up,
                favorite = img_icon_favorite,
                setting = img_icon_setting;
        switch(this.props.activeTab){
            case 'home' :  home = img_icon_home_active; break;
            case 'notification' : notification = img_icon_notification_active; break;
            case 'followUp' : followUp = img_icon_follow_up_active; break;
            case 'favorite' : favorite = img_icon_favorite_active; break;
            case 'setting' : setting = img_icon_setting_active; break;
        }
        return(
            <View style={styles.bottomNavigator}>
                <View style={{flexDirection : 'row',width : "100%",justifyContent : 'center'}}>
                    <TouchableOpacity style={styles.item} onPress={()=>{
                        this.props.navigation.navigate('homeScreen');
                    }}>
                        <Image style={styles.icon} source={home}/>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.item} onPress={()=>{
                        this.props.navigation.navigate('notificationScreen');
                    }}>
                        <Image style={styles.icon} source={notification}/>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.item} onPress={()=>{
                        this.props.navigation.navigate('followUpScreen');
                    }}>
                        <Image style={styles.icon} source={followUp}/>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.item} onPress={()=>{
                        this.props.navigation.navigate('favoriteScreen');
                    }}>
                        <Image style={styles.icon} source={favorite}/>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.item} onPress={()=>{
                        this.props.navigation.navigate('mySettingScreen');
                    }}>
                        <Image style={styles.icon} source={setting}/>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    bottomNavigator : {
        width : '100%',
        justifyContent : 'center',
        height : 70,
        borderTopColor : "#cccccc",
        borderTopWidth : 2,
    },
    item : {
        flex : 0.2,
        textAlign : 'center',
        alignItems : 'center'
    },
    icon : {
        width : 65,
        height : 65
    }
})
export default BottomNavigationBar;