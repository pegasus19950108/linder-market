import React, { Component } from 'react';
import { View, Image, Text, TouchableOpacity, StyleSheet} from 'react-native';

class ButtonAddCircle extends Component {
    render(){
        return(
            <TouchableOpacity onPress={this.props.onAddPress}>
                <View style={styles.container}>
                    <Image
                        style={styles.image}
                        source={require('@assets/images/icon-add-circle.png')}/>
                </View>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    container : {
        margin : 20,
        flex : 1,
        alignItems : 'flex-end'
    },
    image : {
        width : 80,
        height : 80
    }
})
export default ButtonAddCircle;