import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, StyleSheet } from 'react-native';
import { img_arrow_back } from '@constants';
import { color_blue_main } from '@themes/color'

class CommonTopBar extends Component{
    render(){
        const {title} = this.props?this.props:"Default";
        return(
            <View style={{flexDirection:'row'}}>
                <View style={styles.container}>
                {/* <View style={styles.subtitle}></View> */}
                    <Text style={styles.subtitle}>Linde APAC Market Intelligence Portal</Text>
                    <View style={styles.titleContainer}>
                        <TouchableOpacity
                            onPress={()=>{
                                this.props.onBack();
                            }}>
                            <Image style={styles.goBack} source={img_arrow_back}/>
                        </TouchableOpacity>
                        <Text style={styles.titleText}>{title}</Text>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container : {
        flex : 1,
        backgroundColor : "white",
        justifyContent:'center',
        borderBottomColor : "#dddddd",
        borderBottomWidth : 10
    },
    titleContainer : {
        flexDirection : 'row'
    },
    subtitle : {
        color : color_blue_main,
        textAlign : 'center',
        fontSize : 10,
        paddingTop : 10
    },
    goBack : {
        display : 'flex',
        width : 45,
        height : 45
    },
    titleText : {
        display : 'flex',
        fontSize : 20,
        paddingTop : 10,
        paddingBottom : 20
    }
})
export default CommonTopBar;