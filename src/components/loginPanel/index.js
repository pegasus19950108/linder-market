import React, { Component } from 'react';
import { StyleSheet, View, Text, TextInput, Button,TouchableOpacity } from 'react-native';

import { color_blue_main } from '@themes/color';
class LoginPanel extends Component{
    constructor(props){
        super(props);
        this.state={
            email : "johndoe@email.com",
            password : "123456",
            currentTab : "user",
        }
    }
    render(){
        const { email, password, currentTab } = this.state;
        let userTab,manageTab;
        if(currentTab === "user"){
            userTab = styles.activeTab;
        }
        else
            manageTab = styles.activeTab;
        return(
            <View style = {styles.container}>
                <View style={styles.panel}>
                    <View style={{height : 80}}>
                        <View style={{flexDirection : 'row'}}>
                            <TouchableOpacity
                                style={[styles.navBar, userTab]}
                                onPress={()=>{this.setState({currentTab : "user"})}}>
                                <Text>USER</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={[styles.navBar, manageTab,{borderLeftColor : color_blue_main, borderLeftWidth : 1}]}
                                onPress={()=>{this.setState({currentTab : "manage"})}}>
                                <Text>MANAGEMENT</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <Text>Email</Text>
                    <TextInput
                        textContentType="emailAddress"
                        style={styles.input}
                        keyboardType="email-address"
                        value = {email}
                        onChangeText={(text)=>{this.setState({email : text})}}
                        />
                    <Text>Password</Text>
                    <TextInput
                        style={styles.input}
                        keyboardType = 'default'
                        autoCapitalize = 'none'
                        autoCorrect = { false }
                        secureTextEntry = { true }
                        value = {password}
                        onChangeText={(text)=>{this.setState({password : text})}}
                        />
                    <View style={{alignItems : 'center'}}>
                        <TouchableOpacity
                            style={styles.button}
                            onPress={()=>{this.props.onLogin(
                                {
                                    email : email,
                                    password : password
                                }
                            )}}>
                            <View>
                                <Text style={{color : "white"}}>LOGIN</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    };
}

const styles = StyleSheet.create({
    container : {
        flexDirection : "row"
    },
    panel : {
        flex : 0.8,
        backgroundColor : "#d8d8d8",
        padding : 20,
    },
    navBar : {
        flex : 0.5,
        alignItems : "center",
        justifyContent : "center",
        padding : 10,
        borderBottomColor : "transparent",
        borderBottomWidth : 4
    },
    activeTab : {
        borderBottomColor : color_blue_main,
        borderBottomWidth : 4
    },
    input : {
        padding : 5,
        borderBottomWidth : 1,
        borderBottomColor : "gray",
        marginBottom : 20
    },
    button : {
        justifyContent: 'center',
        alignItems: 'center',
        height : 50,
        width : "80%",
        color : "white",
        backgroundColor : color_blue_main,
        borderRadius : 30,
        margin : 20,
    }
})

export default LoginPanel;