import {  createReducer } from 'reduxsauce';

const initialState = {
    header : "Brazil",
    title : "Brazilian Market Cheer New Far-Right President Bolsonaro",
    date : "October 25, 2018 3:16 pm",
    thumbImage : require('@assets/images/templateData/blog-image-01.png'),
    highlight : "image credit example Lorem ipsum dolor sit amet, consectetur adipiscing elit",
    content : ["Right-wing candidate Jair Bolsonaro won the presidential run-off election in Brazil in an expected win over the leftist candidate on Sunday, due to the dissatisfaction of Brazilian voters with corruption and crime and the huge graft scandal at oil firm Petrobras that took place under the rule of the leftists who were in power for most of the past decade and a half.",

    "Markets cheered the election outcome, and shares in Petrobras were up more than 7 percent in pre-trading hours on Monday.",
    
    "Bolsonaro is promising a major change of course in Brazil, saying in his acceptance address that \"We cannot continue flirting with socialism, communism, populism and leftist extremism.\"",
    
    "According to Kenneth Rapoza, Senior Contributor at Forbes, the presidential election was only Bolsonaro’s to lose, and the Petrobras bribery scandal contributed a lot to Brazilian voters not backing centrist candidates in the race."],
    tags : [ "#china", "#oil", "#lorem", "ipsum"],
    like : 123,
    unlike : 23
}

export default createReducer(state = initialState, {});