import {  createReducer } from 'reduxsauce';

const initialState = {
    carousel : [
        {
            subTitle : "United State",
            title : "Big Oil Woos China with $24 Billion Splurge in Old Pirate's Lair",
            tags : ["#china", "#oil", "#lorem", "#ipsum",],
            thumbImage : require('@assets/images/templateData/carousel-1.png'),
        },
        {
            subTitle : "Canada",
            title : "Lorem Ipsum dallas with $100k at North Carolina",
            tags : ["#china", "#gas", "#canada", "#ipsum",],
            thumbImage : require('@assets/images/templateData/carousel-2.png'),
        }
    ],
    thumbs : [
        {
            title : 'COMPETITIVE RADAR',
            thumbImage : require('@assets/images/templateData/competitive-radar.png'),
        },
        {
            title : 'KEY CUSTOMERS',
            thumbImage : require('@assets/images/templateData/key-customers.png'),
        },
        {
            title : 'INDUSTRY INSIGHTS',
            thumbImage : require('@assets/images/templateData/industry-insights.png'),
        },
        {
            title : 'MACRO MOVEMENT',
            thumbImage : require('@assets/images/templateData/macro-movement.png'),
        },
    ]

}

export default createReducer(state = initialState, {});