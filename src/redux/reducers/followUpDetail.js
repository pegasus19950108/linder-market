import {  createReducer } from 'reduxsauce';

const initialState = {
    thumb : {
        header : "China",
        content : "Brazilian Markets Cheer New Far-Right President Bolsonaro",
        tags : ["#Brazil", "#oil"],
        thumbImage : require('@assets/images/templateData/thumb-image-01.png')
    },
    detail : {
        sender : {
            avatar : require('@assets/images/templateData/avatar-01.png'),
            name : "Mary Tan"
        },
        date : "October 25, 2018 3:16 pm",
        state : "completed",
        detailContent : "Right-wing candidate Jair Bolsonaro won the presidential run-off election in Brazil in an expected win over the leftist candidate on Sunday, due to the dissatisfaction of Brazilian voters with corruption and crime and the huge graft scandal at oil firm Petrobras that took place under the rule of the leftists who were in power for most of the past decade and a half. Markets cheered the election outcome, and shares in Petrobras were up more than 7 percent in pre-trading hours on Monday."
    },
    replies : [
        {
            user : {
                avatar : require('@assets/images/templateData/avatar-02.png'),
                name : "John Doe"
            },
            date : "October 25,2018 3:20 pm",
            content : "Noted with Thanks",
        }
    ]
}

export default createReducer(state = initialState, {});