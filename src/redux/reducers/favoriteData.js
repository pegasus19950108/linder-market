import {  createReducer } from 'reduxsauce';

const initialState = {
    currentSection : "OIL & GAS",
    data : [
        {
            country : "China",
            content : "China Says Researchers Find Lower-Cost Way To Make Solar Cells",
            tags : ["#china", "#oil"],
            thumbImage : require('@assets/images/templateData/thumb-image-01.png')
        },
        {
            country : "Brazil",
            content : "Brazilian Markets Cheer New Far-Right President Bolsonaro",
            tags : ["#china", "#oil"],
            thumbImage : require('@assets/images/templateData/thumb-image-02.png')
        }
    ]
}

export default createReducer(state = initialState, {});