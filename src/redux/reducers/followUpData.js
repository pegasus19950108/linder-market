import {  createReducer } from 'reduxsauce';

const initialState = [
    {
        header : "@ Mary sent a follow up",
        title : "China Calls Any Challenge on Taiwan 'Extremely Dangerous'",
        sender : {
            name : "Mary Tan",
            image : require('@assets/images/templateData/avatar-01.png')
        },
        state : "on going",
        content : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam a congue mi. Curabitur euismod..."
    },
    {
        header : "@ Mary sent a follow up",
        title : "China Calls Any Challenge on Taiwan 'Extremely Dangerous'",
        sender : {
            name : "Mary Tan",
            image : require('@assets/images/templateData/avatar-02.png')
        },
        state : "completed",
        content : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam a congue mi. Curabitur euismod..."
    }
]

export default createReducer(state = initialState, {});