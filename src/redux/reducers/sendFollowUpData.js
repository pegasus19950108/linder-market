import {  createReducer } from 'reduxsauce';

const initialState = {
    globalEmails : [
        "johedoe@email.com",
        "johedoe@gmail.com",
        "johedoe@hotmail.com",
        "johedoe@outlook.com",
        "janesmiss@email.com",
        "janesmiss@gmail.com",
        "michalejoe@hotmail.com",
        "jackson@outlook.com",
    ],
    thumb : {
        header : "China",
        content : "Brazilian Markets Cheer New Far-Right President Bolsonaro",
        tags : ["#Brazil", "#oil"],
        thumbImage : require('@assets/images/templateData/thumb-image-02.png')
    }
}

export default createReducer(state = initialState, {});