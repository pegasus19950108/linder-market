import {  createReducer } from 'reduxsauce';

const initialState = {
    currentSection : "OIL & GAS",
    data : [
        {
            country : "China",
            content : "China Says Researchers Find Lower-Cost Way To Make Solar Cells",
            tags : ["#china", "#oil"],
            thumbImage : require('@assets/images/templateData/thumb-image-01.png')
        },
        {
            country : "Brazil",
            content : "Brazilian Markets Cheer New Far-Right President Bolsonaro",
            tags : ["#china", "#oil"],
            thumbImage : require('@assets/images/templateData/thumb-image-02.png')
        },
        {
            country : "Iran",
            content : "EU Struggles To Create Iran Oil Trade Payment Vehicle",
            tags : ["#china", "#oil"],
            thumbImage : require('@assets/images/templateData/thumb-image-03.png')
        },
        {
            country : "United State",
            content : "Large Shareholder Ready Invest More Money Into Tesla",
            tags : ["#china", "#oil"],
            thumbImage : require('@assets/images/templateData/thumb-image-04.png')
        },
        {
            country : "Iraq",
            content : "Iraq Expects Exemption From U.S Sanction Against Iran",
            tags : ["#china", "#oil"],
            thumbImage : require('@assets/images/templateData/thumb-image-05.png')
        },
    ]
}

export default createReducer(state = initialState, {});