import { combineReducers } from 'redux';

import Global from './global';
import HomeData from './homeData';
import DepartmentData from './departmentData';
import SubDepartmentData from './subDepartmentData'
import SectionDetailData from './sectionDetailData';
import SendFollowUpData from './sendFollowUpData';
import SearchResult from './searchResult';
import Notifications from './notifications';
import FavoriteData from './favoriteData';
import FollowUpData from './followUpData';
import FollowUpDetail from './followUpDetail';
import MyProfile from './myProfile';

const AppReducer = combineReducers({
    global : Global,
    homeData : HomeData,
    departmentData : DepartmentData,
    subDepartmentData : SubDepartmentData,
    sectionDetailData : SectionDetailData,
    sendFollowUpData : SendFollowUpData,
    searchResult : SearchResult,
    notifications : Notifications,
    favoriteData : FavoriteData,
    followUpData : FollowUpData,
    followUpDetail : FollowUpDetail,
    myProfile : MyProfile,
});

export default AppReducer;