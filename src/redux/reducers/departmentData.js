import {  createReducer } from 'reduxsauce';

const initialState = {
    departments : ["COMPETITIVE RADAR", "KEY CUSTOMERS", "INDUSTRY INSIGHTS", "MACRO MOVEMENT"],

    currentDepartment : "KEY CUSTOMERS",
    
    thumbs : [
        {
            title : "METAL & GLASS",
            thumbImage : require('@assets/images/templateData/metal-glass.png')
        },
        {
            title : "MANUFACTURING",
            thumbImage : require('@assets/images/templateData/manufacturing.png')
        },
        {
            title : "OIL & GAS",
            thumbImage : require('@assets/images/templateData/oil-gas.png')
        },
        {
            title : "ELECTRONICS",
            thumbImage : require('@assets/images/templateData/electronics.png')
        },
        {
            title : "HEALTHCARE",
            thumbImage : require('@assets/images/templateData/healthcare.png')
        },
    ]
}

export default createReducer(state = initialState, {});