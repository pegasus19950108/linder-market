import {  createReducer } from 'reduxsauce';

const initialState = {
    profile : {
        name : "John Doe",
        email : "johnemail@email.com",
        avatar : require('@assets/images/templateData/avatar-02.png'),
    },
    tags : ["GERMANY", "TAG NAME LOREM", "TAG 1", "TAG", "TAG3", "TAG NAME LOREM"],
    termConditionData : [
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ultricies in quam luctus dignissim. Ut eget urna nec ipsum eleifend commodo. In pellentesque volutpat odio ac facilisis. Quisque sed consequat erat. Sed velit massa, eleifend eget elit sit amet, cursus condimentum ex. Vestibulum quis ornare metus, et dapibus elit. Sed in cursus mauris.",

        "Vivamus facilisis euismod leo, id faucibus metus varius ac. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nam lobortis convallis quam et vehicula. Donec ornare at nunc non rutrum. Nunc ut sem a turpis volutpat pretium vitae quis arcu. Curabitur ut mi nibh. Cras eleifend pretium facilisis.",
        
        "Curabitur vulputate quam nec ex aliquet, vel auctor tortor volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer convallis, nisl a pulvinar congue, mi justo consectetur odio, eu pellentesque metus sapien ac felis. Etiam varius sapien leo, ut mollis dui feugiat vel.",
    ],
    privacyPolicyData : [
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ultricies in quam luctus dignissim. Ut eget urna nec ipsum eleifend commodo. In pellentesque volutpat odio ac facilisis. Quisque sed consequat erat. Sed velit massa, eleifend eget elit sit amet, cursus condimentum ex. Vestibulum quis ornare metus, et dapibus elit. Sed in cursus mauris.",

        "Vivamus facilisis euismod leo, id faucibus metus varius ac. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nam lobortis convallis quam et vehicula. Donec ornare at nunc non rutrum. Nunc ut sem a turpis volutpat pretium vitae quis arcu. Curabitur ut mi nibh. Cras eleifend pretium facilisis.",
        
        "Curabitur vulputate quam nec ex aliquet, vel auctor tortor volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer convallis, nisl a pulvinar congue, mi justo consectetur odio, eu pellentesque metus sapien ac felis. Etiam varius sapien leo, ut mollis dui feugiat vel.",
    ]
}

export default createReducer(state = initialState, {});