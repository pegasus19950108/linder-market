import { createReducer } from 'reduxsauce';

const initialState = {
    loading : false
}

export default createReducer(initialState, {});