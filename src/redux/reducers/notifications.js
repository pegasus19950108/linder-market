import {  createReducer } from 'reduxsauce';

const initialState = [
    {
        header : "@ Mary sent a follow up",
        title : "China Calls Any Challenge on Taiwan 'Extremely Dangerous'",
        sender : {
            name : "Mary Tan",
            image : require('@assets/images/templateData/avatar-01.png')
        },
        content : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam a congue mi. Curabitur euismod..."
    },
    {
        header : "#China",
        title : "China Says Researchers Find Lower-Cost Way To Make Solar Cells"
    },
    {
        header : "@ Mary sent a follow up",
        title : "China Calls Any Challenge on Taiwan 'Extremely Dangerous'",
        sender : {
            name : "Mary Tan",
            image : require('@assets/images/templateData/avatar-02.png')
        },
        content : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam a congue mi. Curabitur euismod..."
    },
    {
        header : "#China",
        title : "China Says Researchers Find Lower-Cost Way To Make Solar Cells"
    },
]

export default createReducer(state = initialState, {});