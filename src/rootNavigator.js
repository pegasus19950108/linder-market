import { createAppContainer, createStackNavigator, createBottomTabNavigator } from 'react-navigation'

import SplashScreen from './containers/splashScreen';
import LoginScreen from './containers/loginScreen';
import ForgotPassword from './containers/forgotPasswordScreen'
import HomeScreen from './containers/HomeScreen';
import DepartmentScreen from './containers/DepartmentScreen';
import SubDepartmentScreen from './containers/SubDepartmentScreen';
import SectionDetailScreen from './containers/sectionDetailScreen';
import SendFollowUp from './containers/SendFollowUp';
import SearchScreen from './containers/searchScreen';
import UploadNewsScreen from './containers/uploadNewsScreen';
import NotificationScreen from './containers/notificationScreen';
import FavoriteScreen from './containers/favoriteScreen';
import FollowUpScreen from './containers/followUpScreen';
import FollowUpDetailScreen from './containers/followUpDetailScreen';
import FollowUpReplyScreen from './containers/followUpReplyScreen';
import MySettingScreen from './containers/mySettingScreen';
import EditProfileScreen from './containers/editProfileScreen';
import ChangePasswordScreen from './containers/changePasswordScreen';
import TermConditionScreen from './containers/termConditionScreen';
import PrivacyPolicyScreen from './containers/privacyPolicyScreen';
import ProfileSettingScreen from './containers/profileSettingScreen';
import EditTagScreen from './containers/editTagScreen';

const AppNavigator = createStackNavigator(
    {
        splashScreen         : { screen : SplashScreen},
        loginScreen          : { screen : LoginScreen },
        forgotPassword       : { screen : ForgotPassword },
        homeScreen           : { screen : HomeScreen },
        departmentScreen     : { screen : DepartmentScreen },
        subDepartmentScreen  : { screen : SubDepartmentScreen },
        sectionDetailScreen  : { screen : SectionDetailScreen },
        sendFollowUp         : { screen : SendFollowUp },
        searchScreen         : { screen : SearchScreen },
        uploadNewsScreen     : { screen : UploadNewsScreen },
        notificationScreen   : { screen : NotificationScreen },
        favoriteScreen       : { screen : FavoriteScreen },
        followUpScreen       : { screen : FollowUpScreen },
        followUpDetailScreen : { screen : FollowUpDetailScreen },
        followUpReplyScreen  : { screen : FollowUpReplyScreen},
        mySettingScreen      : { screen : MySettingScreen },
        editProfileScreen    : { screen : EditProfileScreen },
        changePasswordScreen : { screen : ChangePasswordScreen },
        termConditionScreen  : { screen : TermConditionScreen },
        privacyPolicyScreen  : { screen : PrivacyPolicyScreen },
        profileSettingScreen : { screen : ProfileSettingScreen},
        editTagScreen        : { screen : EditTagScreen},
    },
    {
        initialRouteName : 'splashScreen',
        navigationOptions : {

        },
        headerMode : 'none'
    }
)

const App = createAppContainer(AppNavigator);

export default App;