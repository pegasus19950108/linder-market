import React, {Component} from 'react';
import {StatusBar, StyleSheet, View, Text, TouchableOpacity} from 'react-native';

import { color_blue_main } from '@themes/color';

import LoginPanel from '@components/loginPanel';


export default class App extends Component {
    componentDidMount(){
        StatusBar.setHidden(true);
    }

    onLogin = (loginData) => {
        this.props.navigation.navigate('homeScreen');
    }
    render() {
        return (
            <View style={styles.container}>
                <LoginPanel onLogin={this.onLogin}/>
                <View style={styles.gotoForgot}>
                    <Text style={styles.text}>If you forgot your password , </Text>
                    <TouchableOpacity
                        onPress={()=>{
                            this.props.navigation.navigate('forgotPassword');
                        }}>
                        <Text style={[styles.text,{textDecorationLine : "underline"}]}>click here</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: color_blue_main,
    },
    gotoForgot : {
        flexDirection : 'row',
        paddingTop : 20
    },
    text : {
        display : 'flex',
        color : "white"
    }
});