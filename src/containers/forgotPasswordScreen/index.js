import React, {Component} from 'react';
import {Platform, StyleSheet, View, StatusBar,Text,TextInput, TouchableOpacity} from 'react-native';

import { color_blue_main } from '@themes/color';
import { img_logo } from '@constants'

import CommonTopBar from '@components/CommonTopBar';
import LoginPanel from '@components/loginPanel';
export default class ForgotPassword extends Component {
    constructor(){
        super();
        this.state = {
            email : ""
        }
    }
    componentDidMount(){
        StatusBar.setHidden(true);
    }
    render() {
        const { email } = this.state;
        const disabled = email===""?styles.disable:null;
        return (
            <View style={styles.container}>
                <CommonTopBar title="FORGOT PASSWORD"
                    onBack={()=>{
                        this.props.navigation.navigate('loginScreen');
                    }}
                    />
                
                <TextInput
                    textContentType="emailAddress"
                    style={styles.input}
                    keyboardType="email-address"
                    value = {email}
                    onChangeText={(text)=>{this.setState({email : text})}}
                    />
                <Text style={styles.hint}>New Password will be send to your email</Text>
                <View style={{width:"80%",alignItems : 'center'}}>
                    <TouchableOpacity style={[styles.button,disabled]}>
                        <View>
                            <Text style={{color : "white"}}>SUBMIT</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: "white",
    },
    input : {
        width : "80%",
        padding : 5,
        paddingTop : 20,
        borderBottomWidth : 1,
        borderBottomColor : "gray",
        marginBottom : 20
    },
    hint : {
        textAlign : "left",
        width : "100%",
        paddingLeft : "10%"
    },
    button : {
        justifyContent: 'center',
        alignItems: 'center',
        height : 50,
        width : "80%",
        color : "white",
        backgroundColor : color_blue_main,
        borderRadius : 30,
        margin : 20,
    },
    disable : {
        backgroundColor : '#cccccc'
    }
});