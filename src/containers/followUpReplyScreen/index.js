import React, { Component } from 'react';
import { View, Text,TextInput, Image, StyleSheet,ScrollView, TouchableOpacity } from 'react-native';
import {MenuProvider,MenuContext} from 'react-native-popup-menu';

import BottomNavigationBar from '@components/bottomNavigationBar';

import TopBar from './topBar';

import {connect} from 'react-redux';

class FollowUpReplyScreen extends Component{
    constructor(){
        super();
        this.state={
            currentContent : ""
        }
    }
    
    _onReply = () => {
        alert(this.state.currentContent);
    }

    render(){
        const {currentContent} = this.state;
        return(
            <View style={styles.container}>
                <TopBar onBack={()=>{this.props.navigation.goBack()}} onReply={this._onReply}/>
                <View style={styles.content}>
                    <TextInput
                        multiline={true}
                        style={styles.textInput}
                        value={currentContent}
                        onChangeText={text=>this.setState({currentContent : text})}/>
                </View>
                <BottomNavigationBar navigation={this.props.navigation} activeTab="followUp"/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "white",
    },
    content : {
        flex : 1,
        width : "100%",
        backgroundColor : 'white',
        flexDirection : 'column'
    },
    textInput : {
        flex : 1,
        margin : 20,
        textAlignVertical : 'top'
    }
});

function mapStateToProps(state) {
    return {
        // followUpDetail : state.followUpDetail
    }
}
export default connect(mapStateToProps)(FollowUpReplyScreen);