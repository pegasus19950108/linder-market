import React, { Component } from 'react';
import { View, Text,TextInput, Image, StyleSheet,ScrollView, TouchableOpacity } from 'react-native';
import {MenuProvider,MenuContext} from 'react-native-popup-menu';

import BottomNavigationBar from '@components/bottomNavigationBar';

import TopBar from './topBar';
// import NavBar from './navBar';
// import FollowUpItem from './followUpItem';

import {connect} from 'react-redux';

import { color_blue_main } from '@themes/color';

import {
    img_icon_download
} from '@constants';

class FollowUpDetailScreen extends Component{
    constructor(){
        super();
        this.state={
        }
    }
    render(){
        const {
            thumbImage,
            header,
            content,
            tags
        } = this.props.followUpDetail.thumb;

        const {
            sender,
            state,
            date,
            detailContent
        } = this.props.followUpDetail.detail;

        const replies = this.props.followUpDetail.replies;

        let senderAvatar = (sender.avatar)?(
            <Image source={sender.avatar} style={{display : 'flex',width : 50,height : 50,marginRight:10}}/>
        ):null;

        let senderName = (sender.name)?(
            <Text style={{fontSize:16}}>{sender.name}</Text>
        ):null;

        let detailDate = (date)?(
            <Text style={{fontSize:12,color:color_blue_main}}>{date}</Text>
        ):null;

        let currentStateTagStyle = (state === "on going")?{backgroundColor : color_blue_main}:null;
        let currentStateTextStyle = (state === "on going")?{color : "white"}:null;;

        let detailState = state?(
            <View style={[styles.stateTag, currentStateTagStyle]}>
                <Text style={[styles.stateText, currentStateTextStyle]}>{state.toUpperCase()}</Text>
            </View>
        ):null;

        let repliesElem = (replies&&replies.length)?(
            replies.map((item,i)=>{
                return(
                    <View key={i}>
                        <View style={{flexDirection:'row',flex : 1,marginBottom:10}}>
                            <Image source={item.user.avatar} style={{display : 'flex',width : 50,height : 50,marginRight:10}}/>
                            <View style={{flex : 1,marginTop:4}}>
                                <Text style={{fontSize:16}}>{item.user.name}</Text>
                                <Text style={{fontSize:12,color:color_blue_main}}>{item.date}</Text>
                            </View>
                        </View>
                        <Text style={{lineHeight:18,fontSize:15}}>{item.content}</Text>
                    </View>
                )
            })
        ):null;
        return(
            <View style={styles.container}>
                <TopBar
                    onBack={()=>{
                        this.props.navigation.goBack()
                    }} 
                    gotoReply={()=>{
                        this.props.navigation.navigate('followUpReplyScreen');
                    }}/>
                <View style={styles.content}>
                    <ScrollView>
                        <View style={styles.thumbContainer}>
                            <Image style={styles.thumbImage} source={thumbImage}/>
                            <View style={{flexDirection : 'column',flex : 1,padding : 5}}>
                                <View style={styles.header}>
                                    <Text style={styles.headerText}>{header}</Text>
                                </View>
                                <View style={styles.textContainer}>
                                    <Text style={styles.contentText}>{content}</Text>
                                </View>
                                <View style={styles.tags}>
                                    {tags.map((item,i)=>{
                                        return(
                                            <Text key={i} style={styles.tag}>{item}</Text>
                                        )
                                    })}
                                </View>
                            </View>
                        </View>
                        <View style={styles.btnDownloadPDF}>
                            <TouchableOpacity style={{flexDirection:'row'}}>
                                <Image source={img_icon_download} style={{display:'flex',width : 25,height:25}}/>
                                <Text style={{color:color_blue_main,display:'flex',marginLeft : 10,marginTop:3}}>DOWNLOAD PDF</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.detailContent}>
                            <View style={{flexDirection:'row',flex : 1}}>
                                {senderAvatar}
                                <View style={{flex : 1,marginTop:4}}>
                                    {senderName}
                                    {detailDate}
                                </View>
                                {detailState}
                            </View>
                            <View style={{marginTop:10}}>
                                <Text style={{lineHeight:18,fontSize:15}}>{detailContent}</Text>
                            </View>
                        </View>

                        <View style={styles.replyContent}>
                            {repliesElem}
                        </View>
                        
                    </ScrollView>
                </View>
                <BottomNavigationBar navigation={this.props.navigation} activeTab="followUp"/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "white",
    },
    content : {
        flex : 1,
        width : "100%",
        backgroundColor : '#e8e8e8'
    },
    thumbContainer : {
        flexDirection : 'row',
        backgroundColor : 'white',
        padding : 5,
        borderBottomColor : '#e8e8e8',
        borderBottomWidth : 4
    },
        thumbImage : {
            width : 130,
            height : 130
        },
        header : {
            display : 'flex',
            justifyContent:'flex-start',
            flexDirection : 'row'
        },
        headerText : {
            color : color_blue_main,
            flex : 1,
            fontSize : 14,
            marginTop : 5
        },
        textContainer : {
            display : 'flex',
            flex : 1,
            width : "100%"
        },
        contentText : {
            fontSize : 16
        },
        tags : {
            display : 'flex',
            flexDirection : 'row'
        },
            tag : {
                display : 'flex',
                color : color_blue_main,
                backgroundColor : '#e7effc',
                margin : 4,
                paddingLeft : 4,
                paddingRight : 4
            },
    btnDownloadPDF : {
        flex : 1, 
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
        padding : 20,
        backgroundColor:'white',
        borderBottomColor : '#e8e8e8',
        borderBottomWidth : 4
    },
    detailContent : {
        backgroundColor : 'white',
        padding : 20,
        borderBottomColor : '#e8e8e8',
        borderBottomWidth : 4
    },
        stateTag : {
            justifyContent: 'center',
            alignItems: 'center',
            height : 25,
            // backgroundColor : 'white',
            borderColor : color_blue_main,
            borderWidth : 1,
            borderRadius : 30,
            marginTop : 12,
            width : 100,
            flexDirection : 'row',
        },
        stateText : {
            color : color_blue_main
        },
    replyContent : {
        backgroundColor : 'white',
        padding : 20,
        borderBottomColor : '#e8e8e8',
        borderBottomWidth : 4
    },
});

function mapStateToProps(state) {
    return {
        followUpDetail : state.followUpDetail
    }
}
export default connect(mapStateToProps)(FollowUpDetailScreen);