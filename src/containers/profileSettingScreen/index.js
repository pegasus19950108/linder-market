import React, { Component } from 'react';
import { Platform, View, Text,TextInput, Image, StyleSheet,ScrollView, TouchableOpacity,Switch } from 'react-native';
import {connect} from 'react-redux';

import BottomNavigationBar from '@components/bottomNavigationBar';

import TopBar from './topBar';

import { color_blue_main } from '@themes/color';

class ProfileSettingScreen extends Component{
    constructor(){
        super();
        this.state={
            notification : false
        };
    }

    _notificationSettingChanged = () => {
        this.setState({notification : !this.state.notification})
    }

    _logout = () => {
        alert('logout')
    }
    render(){
        const thumbColor = this.state.notification?color_blue_main:'gray';
        return(
            <View style={styles.container}>
                <TopBar onBack={()=>this.props.navigation.goBack()}/>
                <View style={styles.content}>
                    <View style={styles.pushNotificationSection}>
                        <Text style={{flex : 1,fontSize:18}}>Push Notification</Text>
                        <View style={{display : 'flex'}}>
                            <Switch
                                onValueChange = {()=>this._notificationSettingChanged()}
                                value = {this.state.notification}
                                trackColor={{true:"#b2d1f0",false:"#d8d8d8"}}
                                thumbColor={thumbColor}/>
                        </View>
                    </View>
                    <View style={styles.logoutButtonSection}>
                        <TouchableOpacity
                            onPress={this._logout}>
                            <Text style={styles.logoutButton}>Logout</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <BottomNavigationBar navigation={this.props.navigation} activeTab="setting"/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "white",
    },
    content : {
        flex : 1,
        width : "100%",
    },
    pushNotificationSection : {
        flexDirection : 'row',
        padding : 20,
        borderBottomColor : '#e8e8e8',
        borderBottomWidth : 4,
    },
    logoutButtonSection : {
        padding : 20
    },
    logoutButton : {
        fontSize : 18
    }
});

function mapStateToProps(state) {
    return {
        myProfile : state.myProfile
    }
}
export default connect(mapStateToProps)(ProfileSettingScreen);