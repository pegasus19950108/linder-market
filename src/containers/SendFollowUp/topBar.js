import React, { Component } from 'react'
import { View, TextInput, Text, Image, StyleSheet, TouchableOpacity } from 'react-native';
import { color_blue_main } from '@themes/color';

import {
    img_arrow_back,
} from '@constants';

class TopBar extends Component{
    render(){
        return(
            <View style={styles.container}>
                <Text style={styles.title}>Linder APAC Intelligence Portal</Text>
                <View style={{flexDirection:'row',width:"95%",padding:10}}>
                    <TouchableOpacity>
                        <Image source={img_arrow_back} style={styles.icon}/>
                    </TouchableOpacity>
                    <Text style={styles.input}>FOLLOW UP</Text>
                    <TouchableOpacity>
                        <Text style={{color : color_blue_main,fontSize:18,marginTop:3}}>SEND</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container : {
        width : "100%",
        borderBottomColor : "#dddddd",
        borderBottomWidth : 5,
        justifyContent:'center',
        alignItems : 'center'
    },
    title : {
        color : color_blue_main,
        fontSize : 10,
        textAlign : 'center'
    },
    icon : {
        display : 'flex',
        width : 30,
        height : 30,
        margin : 5,
        marginTop : 0
    },
    input:{
        flex : 1,
        marginTop : 0,
        padding : 5,
        fontSize : 16
    }
})
export default TopBar;