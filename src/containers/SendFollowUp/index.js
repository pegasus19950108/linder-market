import React, { Component } from 'react';
import { View, Text,TextInput, Image, StyleSheet, TouchableOpacity } from 'react-native';
import {
    Menu,
    MenuOptions,
    MenuOption,
    MenuTrigger,
    renderers,
    MenuProvider
  } from 'react-native-popup-menu';

import BottomNavigationBar from '@components/bottomNavigationBar';

import TopBar from './topBar';
import {connect} from 'react-redux';

import { color_blue_main } from '@themes/color';

import {
    img_icon_add,
    img_icon_close
} from '@constants';

class SendFollowUp extends Component{
    constructor(){
        super();
        this.state={
            currentEmail : "",
            emails : [],
            autoCompleteMenuVisible : false,
        }
    }
    _addEmail = () => {
        let emails = this.state.emails;
        if(this.state.currentEmail)
        {
            emails.push(this.state.currentEmail);
            this.setState({emails : emails});
        }
    }

    _removeEmail =(i) =>{
        let emails = this.state.emails;
        emails.splice(i,1)
        this.setState({emails : emails});
    }

    _filterEmail = (email) =>{
        let { globalEmails } = this.props.sendFollowUpData;
        let result = [];
        if(email)
        {
            for(var i in globalEmails){
                if(globalEmails[i].includes(email) === true)
                    result.push(globalEmails[i])
            }
        }
        return result;
    }
    render(){
        const {
            thumbImage,
            header,
            content,
            tags
        } = this.props.sendFollowUpData.thumb;

        const {
            currentEmail,
            emails,
            autoCompleteMenuVisible
        } = this.state;
        const filteredEmails = this._filterEmail(currentEmail);
        return(
            <MenuProvider>
                <View style={styles.container}>
                    <TopBar/>
                    <View style={styles.content}>
                        <View style={styles.thumbContainer}>
                            <Image style={styles.thumbImage} source={thumbImage}/>
                            <View style={{flexDirection : 'column',flex : 1,padding : 5}}>
                                <View style={styles.header}>
                                    <Text style={styles.headerText}>{header}</Text>
                                </View>
                                <View style={styles.textContainer}>
                                    <Text style={styles.contentText}>{content}</Text>
                                </View>
                                <View style={styles.tags}>
                                    {tags.map((item,i)=>{
                                        return(
                                            <Text key={i} style={styles.tag}>{item}</Text>
                                        )
                                    })}
                                </View>
                            </View>
                        </View>
                        <View style={styles.addInfoContainer}>
                            <View style={styles.emailSection}>
                                <View style={{flexDirection : 'row', borderBottomColor : '#d8d8d8', borderBottomWidth : 2,paddingBottom : 10}}>
                                    
                                    <TextInput
                                        style={{flex : 1, padding : 0}}
                                        placeholder="Enter Email..."
                                        value={currentEmail}
                                        onChangeText={(text)=>{
                                            this.setState({currentEmail:text});
                                            if(text)
                                                this.setState({autoCompleteMenuVisible : true})
                                        }}
                                    />
                                    <TouchableOpacity
                                        onPress={this._addEmail}>
                                        <Image source={img_icon_add} style={{width:30,height:30}}/>
                                    </TouchableOpacity>
                                    <Menu
                                        opened={autoCompleteMenuVisible}
                                        onBackdropPress={()=>{this.setState({autoCompleteMenuVisible:false})}}>
                                        <MenuTrigger>
                                        </MenuTrigger>
                                        <MenuOptions optionsContainerStyle={{width : "90%",marginTop : 30}}>
                                            {
                                                filteredEmails.map((item,i)=>{
                                                    return(
                                                        <MenuOption key={i} onSelect={() => {
                                                            this.setState({currentEmail : item})
                                                        }} >
                                                            <Text style={styles.menuItem}>{item}</Text>
                                                        </MenuOption>
                                                    )
                                                })
                                            }
                                        </MenuOptions>
                                    </Menu>
                                </View>
                                <View style={{paddingTop:10}}>
                                    {
                                        emails.map((item,i)=>{
                                            return(
                                                <View key={i} style={styles.emailItem}>
                                                    <Text style={{flex : 1, color :color_blue_main,marginTop:8}}>{item}</Text>
                                                    <TouchableOpacity
                                                        onPress={()=>{this._removeEmail(i)}}>
                                                        <Image source={img_icon_close} style={{display :'flex',width : 30, height:30,marginTop : 3}}/>
                                                    </TouchableOpacity>
                                                </View>
                                            )
                                        })
                                    }
                                </View>
                            </View>
                            <View style={styles.commentSection}>
                                <TextInput
                                    multiline={true}
                                    />
                            </View>
                        </View>
                    </View>
                    <BottomNavigationBar navigation={this.props.navigation} activeTab="home"/>
                </View>
            </MenuProvider>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "white",
    },
    content : {
        flex : 1,
        width : "100%",
        backgroundColor : '#e8e8e8'
    },
    thumbContainer : {
        flexDirection : 'row',
        backgroundColor : 'white',
        padding : 5,
        borderBottomColor : '#e8e8e8',
        borderBottomWidth : 2
    },
        thumbImage : {
            width : 130,
            height : 130
        },
        header : {
            display : 'flex',
            justifyContent:'flex-start',
            flexDirection : 'row'
        },
        headerText : {
            color : color_blue_main,
            flex : 1,
            fontSize : 14,
            marginTop : 5
        },
        textContainer : {
            display : 'flex',
            flex : 1,
            width : "100%"
        },
        contentText : {
            fontSize : 16
        },
        tags : {
            display : 'flex',
            flexDirection : 'row'
        },
            tag : {
                display : 'flex',
                color : color_blue_main,
                backgroundColor : '#e7effc',
                margin : 4,
                paddingLeft : 4,
                paddingRight : 4
            },
    addInfoContainer : {
        flex : 1,
        flexDirection : 'column'
    },
    emailSection : {
        display : 'flex',
        backgroundColor : "white",
        padding : 20,
        borderBottomColor : "#e8e8e8",
        borderBottomWidth : 4
    },
    emailItem : {
        justifyContent: 'center',
        height : 40,
        color : "white",
        borderColor : color_blue_main,
        borderWidth : 1,
        borderRadius : 30,
        marginTop : 5,
        marginBottom : 5,
        paddingLeft : 20,
        paddingRight : 10,
        flexDirection : 'row'
    },
    commentSection : {
        flex : 1,
        backgroundColor : "white"
    },
    menuItem : {
        width : "100%",
        padding : 5,
        paddingLeft : 10
    }
});

function mapStateToProps(state) {
    return {
        sendFollowUpData : state.sendFollowUpData
    }
}
export default connect(mapStateToProps)(SendFollowUp);