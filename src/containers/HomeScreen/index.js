import React, {Component} from 'react';
import {Platform, StyleSheet, View, StatusBar,Text, TouchableOpacity, ScrollView} from 'react-native';

import Carousel from 'react-native-looped-carousel';

import BottomNavigationBar from '@components/bottomNavigationBar';
import ButtonAddCircle from '@components/buttonAddCircle';

import SearchBar from './searchBar';
import Thumb from './thumb';
import CarouselItem from './carouselItem';

import { connect } from 'react-redux';

import { color_blue_main } from '@themes/color';

class HomeScreen extends Component {
    constructor(props){
        super(props);
        this.state = {
            email : ""
        }
    }
    componentDidMount(){
        StatusBar.setHidden(true);
    }
    _onLayoutDidChange = (e) => {
        const layout = e.nativeEvent.layout;
        this.setState({ carouselSize: { width: layout.width, height: 300 } });
      }

    thumbPressed = (data) =>{
        this.props.navigation.navigate('departmentScreen');
    }
    render() {
        const { email } = this.state;
        const { carousel, thumbs } = this.props.homeData;
        return (
            <View style={styles.container}>
                <SearchBar onSearchPress={()=>{this.props.navigation.navigate('searchScreen')}}/>
                <View style={styles.content} onLayout={this._onLayoutDidChange}>
                    <ScrollView style={styles.scrollContainer}>
                        <Carousel
                            delay={2000}
                            style={this.state.carouselSize}
                            autoplay
                            bullets={true}
                            bulletStyle={styles.bulletStyle}
                            bulletsContainerStyle={styles.bulletsContainerStyle}
                            chosenBulletStyle={styles.chosenBulletStyle}
                            >
                            {/* <View style={[{ backgroundColor: '#BADA55' }, this.state.carouselSize]}><Text>1</Text></View>
                            <View style={[{ backgroundColor: 'red' }, this.state.carouselSize]}><Text>2</Text></View>
                            <View style={[{ backgroundColor: 'blue' }, this.state.carouselSize]}><Text>3</Text></View> */}
                            {
                                carousel.map((item,i) => {
                                    return(
                                        <View
                                            key={i}
                                            style={this.state.carouselSize}
                                            >
                                            <CarouselItem data={item}/>
                                        </View>
                                    )
                                })
                            }
                        </Carousel>
                        <View style={styles.thumbContainer}>
                            {
                                thumbs.map((item,i) =>{
                                    return(
                                        <Thumb key={i} data={item} onPress={this.thumbPressed}/>
                                    )
                                })
                            }
                        </View>
                        <ButtonAddCircle onAddPress={()=>{this.props.navigation.navigate('uploadNewsScreen')}}/>
                    </ScrollView>
                </View>
                <BottomNavigationBar navigation={this.props.navigation} activeTab="home"/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "white",
    },
    scrollContainer : {
        position : 'relative'
    },
    content : {
        flex : 1,
        width : "100%",
        backgroundColor : '#e8e8e8'
    },
    carousel : {
        width : 300,
        height : 200
    },
    bulletStyle : {
        backgroundColor : "white",
        margin : 5
    },
    bulletsContainerStyle : {
        
    },
    chosenBulletStyle : {
        backgroundColor : color_blue_main,
        margin : 5
    },
    thumbContainer : {
        flex : 1,
        flexDirection : 'row',
        flexWrap : 'wrap',
    },
    thumb : {
        width : "50%"
    }
});

function mapStateToProps(state) {
    return {
        homeData : state.homeData
    }
}
export default connect(mapStateToProps)(HomeScreen);