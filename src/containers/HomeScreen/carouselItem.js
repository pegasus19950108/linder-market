import React, { Component } from 'react';
import { View, Image, Text, StyleSheet } from 'react-native';

class CarouselItem extends Component {
    render(){
        const { thumbImage, subTitle, title, tags } = this.props.data;
        return(
            <View style={StyleSheet.container}>
                <Image style={styles.thumbImage} source={thumbImage}/>
                <Image style={styles.gradient} source={require('@assets/images/gradient-background.png')}/>
                <View style={styles.contentContainer}>
                    <Text style={styles.subTitle}>{subTitle}</Text>
                    <Text style={styles.title}>{title}</Text>
                    <View style={{flexDirection : 'row'}}>
                        {
                            tags.map((item,i)=>{
                                return(
                                    <Text key={i} style={styles.tag}>{item}</Text>
                                )
                            })
                        }
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container : {
        width : "100%",
        position : "relative"
    },
    thumbImage : {
        width : "100%",
        height : 300
    },
    contentContainer : {
        position : "absolute",
        bottom : 0,
        padding : 20,
        paddingBottom : 40
    },
    gradient : {
        position : 'absolute',
        width : '100%',
        height : 150,
        bottom : 0
    },
    subTitle : {
        fontSize : 13,
        color : "white",
        marginBottom : 10
    },
    title : {
        fontSize : 20,
        color : 'white'
    },
    tag : {
        display : 'flex',
        marginRight : 5,
        padding : 5,
        paddingTop : 0,
        paddingBottom : 0,
        backgroundColor : "gray",
        color : "white"
    }
});

export default CarouselItem;