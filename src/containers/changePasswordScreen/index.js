import React, { Component } from 'react';
import { Platform, View, Text,TextInput, Image, StyleSheet,ScrollView, TouchableOpacity } from 'react-native';
import {connect} from 'react-redux';

import BottomNavigationBar from '@components/bottomNavigationBar';

import TopBar from './topBar';

import { color_blue_main } from '@themes/color';

class ChangePasswordScreen extends Component{
    constructor(){
        super();
        this.state={
            currentPassword : "",
            newPassword : "",
            confirmPassowrd : ""
        }
    }

    _saveProfile = () => {
        alert('save profile');
    }

    render(){
        const { currentPassword, newPassword, confirmPassowrd } = this.state
        return(
            <View style={styles.container}>
                <TopBar onBack={()=>this.props.navigation.goBack()}/>
                <View style={styles.content}>
                    <View style={styles.panel}>
                        <TextInput
                            placeholder="Current Password"
                            style={styles.input}
                            secureTextEntry = { true }
                            value = {currentPassword}
                            onChangeText={(text)=>{this.setState({currentPassword : text})}}
                            />
                        <TextInput
                            placeholder="New Password"
                            style={styles.input}
                            secureTextEntry = { true }
                            value = {newPassword}
                            onChangeText={(text)=>{this.setState({newPassword : text})}}
                            />
                        <TextInput
                            placeholder="Confirm New Password"
                            style={styles.input}
                            secureTextEntry = { true }
                            value = {confirmPassowrd}
                            onChangeText={(text)=>{this.setState({confirmPassowrd : text})}}
                            />
                        <View style={{alignItems : 'center'}}>
                            <TouchableOpacity
                                style={styles.button}
                                onPress={()=>{ }}>
                                <View>
                                    <Text style={{color : "white"}}>UPDATE</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <BottomNavigationBar navigation={this.props.navigation} activeTab="setting"/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "white",
    },
    content : {
        flex : 1,
        width : "100%",
    },
    panel : {
        flex : 0.8,
        padding : 20,
    },
    input : {
        padding : 5,
        borderBottomWidth : 1,
        borderBottomColor : "#d8d8d8",
        marginBottom : 30,
        fontSize : 16
    },
    button : {
        justifyContent: 'center',
        alignItems: 'center',
        height : 50,
        width : "80%",
        color : "white",
        backgroundColor : color_blue_main,
        borderRadius : 30,
        margin : 20,
    }
});

function mapStateToProps(state) {
    return {
        myProfile : state.myProfile
    }
}
export default connect(mapStateToProps)(ChangePasswordScreen);