import React, { Component } from 'react'
import { View, TextInput, Text, Image, StyleSheet, ScrollView, TouchableOpacity } from 'react-native';
import { color_blue_main } from '@themes/color';
import Modal from 'react-native-modal';

import {
    img_arrow_back,
    img_icon_search,
    img_icon_earth
} from '@constants';

const countries = [
    "Brazil",
    "China",
    "Iran",
    "Iraq",
    "Ukraine",
    "United Arab Emirate",
    "United Kingdom",
    "United State",
    "Uruguay"
]
class SearchBar extends Component{
    constructor(){
        super();
        this.state ={
            isModalVisible : false,
            currentCountry : "All Country"
        }
    }
    _toggleModal = () =>{
        this.setState({ isModalVisible: !this.state.isModalVisible });
    }

    _hideModal = () => {
        this.setState({ isModalVisible: false });
    }
    _showModal = () => {
        this.setState({ isModalVisible: true });
    }
    _countryChanged = (country) => {
        this.setState({currentCountry : country});
        this._hideModal();
    }
    render(){
        const section = this.props.title;
        const currentCountry = this.state.currentCountry;
        return(
            <View style={styles.container}>
                <Text style={styles.title}>Linder APAC Intelligence Portal</Text>
                <View style={{flexDirection:'row',width:"95%",padding:10}}>
                    <TouchableOpacity onPress={()=>{this.props.onBack()}}>
                        <Image source={img_arrow_back} style={styles.icon}/>
                    </TouchableOpacity>
                    <Text style={styles.section}>{section}</Text>
                    <TouchableOpacity
                        style={{flexDirection:'row'}}
                        onPress={this._showModal}>
                        <Text style={{display : 'flex',fontSize:14,marginTop : 7}}>{currentCountry}</Text>
                        <Image source={img_icon_earth} style={styles.icon}/>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={this.props.onSearchPress}>
                        <Image source={img_icon_search} style={styles.icon}/>
                    </TouchableOpacity>
                </View>
                <Modal
                    isVisible={this.state.isModalVisible}
                    onRequestClose={this._hideModal}
                    onBackButtonPress={this._hideModal}
                    onBackdropPress={this._hideModal}>
                    <View style={{ flex: 1,flexDirection : 'column',justifyContent:'flex-end'}}>
                        <View style={{flex : 0.5, backgroundColor:'white'}}>
                            <ScrollView style={styles.scrollContainer}>
                                {countries.map((item,i) => {
                                    return(
                                        <TouchableOpacity
                                            key={i}
                                            onPress={()=>{this._countryChanged(item)}}>
                                            <Text style={styles.countryItem}>{item}</Text>
                                        </TouchableOpacity>
                                    )
                                })}
                            </ScrollView>
                        </View>
                    </View>
                </Modal>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container : {
        width : "100%",
        borderBottomColor : "#dddddd",
        borderBottomWidth : 5,
        justifyContent:'center',
        alignItems : 'center'
    },
    title : {
        color : color_blue_main,
        fontSize : 10,
        textAlign : 'center'
    },
    icon : {
        display : 'flex',
        width : 30,
        height : 30,
        margin : 5,
        marginTop : 2
    },
    section:{
        flex : 1,
        padding : 5,
        fontSize : 18
    },
    scrollContainer : {
        // padding : 20
    },
    countryItem : {
        paddingLeft : 30,
        padding : 15,
        fontSize : 20
    }
})
export default SearchBar;