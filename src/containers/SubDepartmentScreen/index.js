import React, {Component} from 'react';
import {Platform, StyleSheet, View, StatusBar,Text, TouchableOpacity, ScrollView} from 'react-native';
import {MenuProvider,MenuContext} from 'react-native-popup-menu';

import BottomNavigationBar from '@components/bottomNavigationBar';
import ButtonAddCircle from '@components/buttonAddCircle';

import SearchBar from './searchBar';
// import NavBar from './navBar';

import Thumb from './thumb';

import { connect } from 'react-redux';

import { color_blue_main } from '@themes/color';

class SubDepartmentScreen extends Component {
    constructor(props){
        super(props);
        this.state = {
            email : ""
        }
    }
    componentDidMount(){
        StatusBar.setHidden(true);
    }
    thumbPressed = (data) =>{
        this.props.navigation.navigate('sectionDetailScreen');
    }
    render() {
        const { currentSection,data } = this.props.subDepartmentData;
        return (
            <MenuProvider
                skipInstanceCheck={true}>
            <View style={styles.container}>
                <SearchBar title={currentSection} onBack={()=>{this.props.navigation.goBack()}} onSearchPress={()=>{this.props.navigation.navigate('searchScreen')}}/>
                <View style={styles.content}>
                    <ScrollView style={styles.scrollContainer}>
                        <View style={styles.thumbContainer}>
                            {
                                data.map((item,i) =>{
                                    return(
                                        <Thumb key={i} data={item} onPress={this.thumbPressed}/>
                                    )
                                })
                            }
                        </View>
                        <ButtonAddCircle onAddPress={()=>{this.props.navigation.navigate('uploadNewsScreen')}}/>
                    </ScrollView>
                </View>
                <BottomNavigationBar navigation={this.props.navigation} activeTab="home"/>
            </View>
            </MenuProvider>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "white",
    },
    content : {
        flex : 1,
        width : "100%"
    },
    scrollContainer : {
        position : 'relative',
        backgroundColor : '#e8e8e8'
    },
    thumbContainer : {
        width : "100%"
        // flex : 1,
        // flexDirection : 'row',
        // flexWrap : 'wrap',
    },
});

function mapStateToProps(state) {
    return {
        subDepartmentData : state.subDepartmentData
    }
}
export default connect(mapStateToProps)(SubDepartmentScreen);