import React, { Component } from 'react';
import { View, Text,TextInput, Image, StyleSheet,ScrollView, TouchableOpacity } from 'react-native';

import BottomNavigationBar from '@components/bottomNavigationBar';

import TopBar from './topBar';
import NotificationItem from './notificationItem';

import {connect} from 'react-redux';

import { color_blue_main } from '@themes/color';

import {
    img_icon_add,
    img_icon_close
} from '@constants';

class NotificationScreen extends Component{
    constructor(){
        super();
        this.state={
            currentEmail : "",
            emails : [],
            autoCompleteMenuVisible : false,
        }
    }
    render(){
        const { notifications } = this.props;
        return(
            <View style={styles.container}>
                <TopBar/>
                <View style={styles.content}>
                    <ScrollView>
                        {
                            notifications.map((item,i)=>{
                                return(
                                    <NotificationItem key={i} data={item}/>
                                )
                            })
                        }
                    </ScrollView>
                </View>
                <BottomNavigationBar navigation={this.props.navigation} activeTab="notification"/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "white",
    },
    content : {
        flex : 1,
        width : "100%",
        backgroundColor : '#e8e8e8'
    },
    thumbContainer : {
        flexDirection : 'row',
        backgroundColor : 'white',
        padding : 5,
        borderBottomColor : '#e8e8e8',
        borderBottomWidth : 2
    },
});

function mapStateToProps(state) {
    return {
        notifications : state.notifications
    }
}
export default connect(mapStateToProps)(NotificationScreen);