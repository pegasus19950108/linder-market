import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, StyleSheet } from 'react-native';

import { color_blue_main } from '@themes/color';

class NotificationItem extends Component{
    render(){
        const { 
            header,
            title,
            sender,
            content
        } = this.props.data;

        let senderElem = sender?(
            <View style={styles.senderSection}>
                <Image source={sender.image} style={styles.senderAvatar}/>
                <Text style={styles.senderName}>{sender.name}</Text>
            </View>
        ):null;

        let contentElem = (
            <Text style={styles.textContent}>{content}</Text>
        )
        
        return(
            <View style={styles.container}>
                <Text style={styles.textHeader}>{header}</Text>
                <Text style={styles.textTitle}>{title}</Text>
                {senderElem}
                {contentElem}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container : {
        backgroundColor : 'white',
        borderBottomColor : "#e8e8e8",
        borderBottomWidth : 4,
        padding : 15,
        paddingLeft : 30,
        paddingRight : 30
    },
    textHeader : {
        color : color_blue_main,
        marginBottom : 10
    },
    textTitle : {
        fontSize : 18,
    },
    textContent : {
        color : "#a7a7a7",
        fontSize : 12
    },
    senderSection : {
        flex : 1,
        flexDirection : 'row',
        paddingTop : 10,
        paddingBottom : 10,
    },
    senderAvatar : {
        display : 'flex',
        width : 40,
        height : 40,
        borderColor : '#d8d8d8',
        borderWidth : 1,
        borderRadius : 20
    },
    senderName : {
        display : 'flex',
        fontSize : 16,
        paddingLeft : 10,
        paddingTop : 7
    }
})
export default NotificationItem;