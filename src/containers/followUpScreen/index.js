import React, { Component } from 'react';
import { View, Text,TextInput, Image, StyleSheet,ScrollView, TouchableOpacity } from 'react-native';
import {MenuProvider,MenuContext} from 'react-native-popup-menu';

import BottomNavigationBar from '@components/bottomNavigationBar';

import TopBar from './topBar';
import NavBar from './navBar';
import FollowUpItem from './followUpItem';

import {connect} from 'react-redux';

import { color_blue_main } from '@themes/color';

import {
    img_icon_add,
    img_icon_close
} from '@constants';

class FollowUpScreen extends Component{
    constructor(){
        super();
        this.state={
            currentEmail : "",
            emails : [],
            autoCompleteMenuVisible : false,
            navData : ["SENT", "RECEIVED"],
            currentNav : "RECEIVED",
        }
    }

    _onFollowUpItemPressed = (item) => {
        this.props.navigation.navigate('followUpDetailScreen');
    }
    render(){
        const { navData, currentNav } = this.state;
        const { followUpData } = this.props;
        return(
            <View style={styles.container}>
                <TopBar/>
                <NavBar data={navData} current={currentNav}/>
                <View style={styles.content}>
                    <ScrollView>
                        {
                            followUpData.map((item,i) =>{
                                return(
                                    <FollowUpItem key={i} data={item} onPress={this._onFollowUpItemPressed}/>
                                )
                            })
                        }
                    </ScrollView>
                </View>
                <BottomNavigationBar navigation={this.props.navigation} activeTab="followUp"/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "white",
    },
    content : {
        flex : 1,
        width : "100%",
        backgroundColor : '#e8e8e8'
    },
    thumbContainer : {
        flexDirection : 'row',
        backgroundColor : 'white',
        padding : 5,
        borderBottomColor : '#e8e8e8',
        borderBottomWidth : 2
    },
});

function mapStateToProps(state) {
    return {
        followUpData : state.followUpData
    }
}
export default connect(mapStateToProps)(FollowUpScreen);