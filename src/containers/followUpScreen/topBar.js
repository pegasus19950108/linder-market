import React, { Component } from 'react'
import { View, TextInput, Text, Image, StyleSheet, TouchableOpacity } from 'react-native';
import { color_blue_main } from '@themes/color';

import {
    img_arrow_back,
} from '@constants';

class TopBar extends Component{
    render(){
        return(
            <View style={styles.container}>
                <Text style={styles.title}>Linder APAC Intelligence Portal</Text>
                <View style={{flexDirection:'row',width:"95%",padding:10}}>
                    <Text style={styles.input}>Follow Up</Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container : {
        width : "100%",
        borderBottomColor : "#dddddd",
        borderBottomWidth : 5,
        justifyContent:'center',
        alignItems : 'center'
    },
    title : {
        color : color_blue_main,
        fontSize : 10,
        textAlign : 'center'
    },
    icon : {
        display : 'flex',
        width : 30,
        height : 30,
        margin : 5,
        marginTop : 0
    },
    input:{
        flex : 1,
        marginTop : 0,
        padding : 5,
        fontSize : 22,
        color : color_blue_main
    }
})
export default TopBar;