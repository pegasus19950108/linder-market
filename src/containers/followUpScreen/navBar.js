import React, { Component } from 'react';
import { View, Text, ScrollView,StyleSheet, TouchableOpacity } from 'react-native';

import { color_blue_main } from '@themes/color';

class NavBar extends Component{
    render(){
        const data = this.props.data;
        const current = this.props.current;
        return(
            <View style={styles.container}>
                {
                    data.map((item,i)=>{
                        const isActive = (item === current)?styles.active:null;
                        return(
                            <View key={i}>
                                <TouchableOpacity>
                                    <Text style={[styles.item,isActive]}>{item}</Text>
                                </TouchableOpacity>
                            </View>
                        )
                    })
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container : {
        width : "100%",
        borderBottomColor : "#d8d8d8",
        borderBottomWidth : 5,
        flexDirection : 'row',
        justifyContent : 'center',
        alignItems : 'center'
    },
    item : {
        display : 'flex',
        padding : 20,
        paddingBottom : 15,
        marginLeft : 20,
        marginRight : 20,
    },
    active : {
        borderBottomColor : color_blue_main,
        borderBottomWidth : 5
    }
})
export default NavBar;