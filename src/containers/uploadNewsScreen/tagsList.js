import React, { Component } from 'react';
import { View,Text, TextInput, Image, StyleSheet, TouchableOpacity} from 'react-native';

import { color_blue_main } from '@themes/color';

import {
    img_icon_add,
    img_icon_close
} from '@constants';
class TagsList extends Component {
    constructor(){
        super();
        this.state={
            currentTag : '',
            tags : []
        }
    }

    _addTag = () => {
        let tags = this.state.tags;
        if(this.state.currentTag)
        {
            tags.push(this.state.currentTag);
            this.setState({tags : tags});
        }
    }
    _removeTag = (i) => {
        let tags = this.state.tags;
        tags.splice(i,1);
        this.setState({tags : tags});
    }

    render(){
        const { currentTag, tags } = this.state;
        return(
            <View style={styles.container}>
                <View style={styles.inputContainer}>
                    <TextInput style={{flex : 1}} placeholder="Add tag..." value={currentTag} onChangeText={text => this.setState({currentTag : text})}/>
                    <TouchableOpacity onPress={this._addTag}>
                        <Image source={img_icon_add} style={styles.icon}/>
                    </TouchableOpacity>
                </View>
                <View style={styles.tagContainer}>
                    {
                        tags.map((item, i)=>{
                            return(
                                <View key={i} style={styles.tag}>
                                    <Text style={styles.tagText}>{item}</Text>
                                    <TouchableOpacity onPress={()=>this._removeTag(i)}>
                                        <Image source={img_icon_close} style={styles.tagClose}/>
                                    </TouchableOpacity>
                                </View>
                            )
                        })
                    }
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex : 1,
        alignItems : 'center',
        backgroundColor: "white",
    },
    inputContainer : {
        width : "90%", 
        flexDirection : 'row', 
        borderBottomColor : '#a9a9a9',
        borderBottomWidth : 1,
        marginBottom : 10
    },
    icon : {
        width : 30,
        height : 30,
        marginTop : 12
    },
    tagContainer : {
        width : "90%",
        alignItems : 'flex-start',
        textAlign : 'left',
        flexDirection : 'row',
        flexWrap : 'wrap',
        flex : 1
    },
    tag : {
        justifyContent: 'center',
        alignItems: 'center',
        height : 40,
        backgroundColor : 'white',
        borderColor : color_blue_main,
        borderWidth : 1,
        borderRadius : 30,
        margin : 5,
        paddingLeft : 15,
        paddingRight : 8,
        flexDirection : 'row',
    },
    tagText : {
        display : 'flex',
        color : color_blue_main,
    },
    tagClose : {
        display : 'flex',
        width : 30,
        height : 30,
    }
});

export default TagsList;