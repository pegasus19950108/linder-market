import React, {Component} from 'react';
import {Platform, StyleSheet, View, StatusBar,Text, TouchableOpacity, ScrollView,TextInput} from 'react-native';
import {MenuProvider,MenuContext} from 'react-native-popup-menu';

import BottomNavigationBar from '@components/bottomNavigationBar';
import ButtonAddCircle from '@components/buttonAddCircle';

import TopBar from './topBar';
import SelectCategory from './selectCategories'
import AddImage from './addImage';
import TagsList from './tagsList';

import { connect } from 'react-redux';

import { color_blue_main } from '@themes/color';

class UploadNewsScreen extends Component {
    constructor(props){
        super(props);
        this.state = {
            email : ""
        }
    }
    componentDidMount(){
        StatusBar.setHidden(true);
    }
    render() {
        return (
            <MenuProvider
                skipInstanceCheck={true}>
            <View style={styles.container}>
                <TopBar onBack={()=>{this.props.navigation.goBack()}} onUploadPress={()=>{}}/>
                <SelectCategory/>
                <View style={styles.content}>
                    <ScrollView style={styles.scrollContainer}>
                        <View style={styles.titleContainer}>
                            <TextInput style={styles.titleText} placeholder="Enter your Title..."/>
                        </View>
                        <View style={styles.contentsContainer}>
                            <TextInput style={styles.contentsText} placeholder="Enter your contents..." multiline={true}/>
                        </View>
                        <AddImage/>
                        <TagsList/>
                    </ScrollView>
                </View>
                <BottomNavigationBar navigation={this.props.navigation} activeTab="home"/>
            </View>
            </MenuProvider>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "white",
    },
    content : {
        flex : 1,
        width : "100%"
    },
    scrollContainer : {
        position : 'relative',
        backgroundColor : 'white'
    },
    titleContainer : {
        backgroundColor : "white",
        borderBottomColor : '#e8e8e8',
        borderBottomWidth : 4
    },
    titleText : {
        fontSize : 20,
        margin : 10
    },
    contentsContainer : {
        backgroundColor : "white",
        borderBottomColor : '#e8e8e8',
        borderBottomWidth : 4,
        justifyContent : 'flex-start'
    },
    contentsText : {
        height : 200,
        textAlignVertical : 'top',
        margin : 10
    }
});

function mapStateToProps(state) {
    return {
        subDepartmentData : state.subDepartmentData
    }
}
export default connect(mapStateToProps)(UploadNewsScreen);