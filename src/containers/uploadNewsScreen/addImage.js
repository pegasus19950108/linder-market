import React, { Component } from 'react';
import {Platform, View,Text, TextInput, Image, StyleSheet, TouchableOpacity, PermissionsAndroid} from 'react-native';
import Modal from 'react-native-modal';

import ImagePicker from 'react-native-image-picker';
import ImageResizer from 'react-native-image-resizer';

import { color_blue_main } from '@themes/color';

import {
    img_icon_camera,
} from '@constants';
class AddImage extends Component {
    constructor(){
        super();
        this.state={
            isModalVisible : false,
            image : null
        }
    }

    _takeImage = (option) => {
        this._hideModal();
        const options = {
            quality: 1.0,
            storageOptions: {
              skipBackup: true,
              path: 'images',
            },
        };
        switch (option) {
            case 'camera':
                ImagePicker.launchCamera(options, (response) => {
                    this.onImagePicker(response, 'FromCamera');
                });
                break;
            case 'gallery':
                ImagePicker.launchImageLibrary(options, (response) => {
                    this.onImagePicker(response, 'FromLibrary');
                });
                break;
            default:
          }
    }

    onImagePicker = (response, whereFrom) => {
        if (response.didCancel) {
            alert('User cancelled image picker');
        } else if (response.error) {
            alert('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
            alert('User tapped custom button: ', response.customButton);
        } else if (response.uri !== undefined) {
            let source = '';

            if (Platform.OS === 'android') {
                source = { uri: response.uri };
            } else {
                source = { uri: response.uri.replace('file://', ''), isStatic: true };
            }

            var degree = 0;
            // if(whereFrom === 'FromCamera') {
            //     degree = Platform.OS === 'android' ? 90 : 0;
            // }

            // this.setState({image : source.uri});
            ImageResizer.createResizedImage(source.uri, 200, 200, 'JPEG', 100, degree)
                .then((resizedImageUri) => {
                    this.setState({
                        image: resizedImageUri,
                    });
                }).catch((err) => {
                    alert(err);
                });
        }
    }

    _hideModal = () => {
        this.setState({isModalVisible : false})
    }
    render(){
        const cameraIcon = (
            <TouchableOpacity onPress={()=>this.setState({isModalVisible : true})}>
                <Image source={img_icon_camera} style={styles.cameraIcon}/>
            </TouchableOpacity>
        )
        const imageData = (
            <TouchableOpacity onPress={()=>this.setState({isModalVisible : true})}>
                <Image source={this.state.image} style={styles.image}/>
            </TouchableOpacity>
        )
        let imageElem = cameraIcon;
        if(this.state.image) imageElem = imageData;

        return(
            <View style={styles.container}>
                {imageElem}
                <TextInput style={styles.input} placeholder="Enter your media's credit (if any)"/>
                <Modal
                    isVisible={this.state.isModalVisible}
                    onRequestClose={this._hideModal}
                    onBackButtonPress={this._hideModal}
                    onBackdropPress={this._hideModal}>
                    <View style={{ flex: 1,flexDirection : 'column',justifyContent:'flex-end'}}>
                        <View style={{display : 'flex', backgroundColor:'white'}}>
                            <TouchableOpacity
                                onPress={()=>{this._takeImage('camera')}}>
                                <Text style={styles.modalItem}>From Camera</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={()=>{this._takeImage('gallery')}}>
                                <Text style={styles.modalItem}>From Gallery</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex : 1,
        flexDirection : 'row',
        alignItems : 'center',
        backgroundColor: "white",
        borderBottomColor : "#e8e8e8",
        borderBottomWidth : 4,
        padding : 5
    },
    cameraIcon : {
        display : 'flex',
        width : 40,
        height : 40,
        margin : 30,
    },
    image : {
        display : 'flex',
        width : 90,
        height : 70,
    },
    input : {
        flex : 1
    },
    modalItem : {
        paddingLeft : 30,
        padding : 15,
        fontSize : 20
    }
});

export default AddImage;