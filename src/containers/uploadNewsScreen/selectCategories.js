import React, { Component } from 'react'
import { View, TextInput, Text, Image, StyleSheet,ScrollView, TouchableOpacity } from 'react-native';
import Modal from 'react-native-modal';

import { color_blue_main } from '@themes/color';

import {
    img_icon_category,
    img_icon_earth
} from '@constants';

const countries = [
    "Brazil",
    "China",
    "Iran",
    "Iraq",
    "Ukraine",
    "United Arab Emirate",
    "United Kingdom",
    "United State",
    "Uruguay"
]

const categories = [
    "Competitive Radar",
    "Key Customers",
    "Industry Insights",
    "Macro Movement"
];

class SelectCategories extends Component{
    constructor(){
        super();
        this.state={
            isCountryModalVisible : false,
            isCategoryModalVisible : false,
            currentCountry : "Select Country",
            currentCategory : "Select Category"
        }
    }
    _hideCountryModal = () => {
        this.setState({isCountryModalVisible : false});
    }

    _hideCategoryModal = () => {
        this.setState({isCategoryModalVisible : false});
    }
    _countryChanged = (country) =>{
        this.setState({currentCountry : country});
        this._hideCountryModal();
    }

    _categoryChanged = (category) => {
        this.setState({currentCategory : category});
        this._hideCategoryModal();
    }

    render(){
        const country = this.state.currentCountry;
        const category = this.state.currentCategory;
        return(
            <View style={styles.container}>
                <View style={{flexDirection:'row',width:"95%",padding:10}}>
                    <TouchableOpacity style={{flex : 1, flexDirection:'row'}}
                        onPress={()=>{
                            this.setState({isCategoryModalVisible : true})
                        }}>
                        <Image source={img_icon_category} style={styles.icon}/>
                        <Text style={{flex : 1,padding : 2, paddingTop : 11, fontSize : 12}}>{category}</Text>
                    </TouchableOpacity>
                    
                    <TouchableOpacity style={{flex : 1, flexDirection:'row'}}
                        onPress={()=>{
                            this.setState({isCountryModalVisible : true})
                        }}>
                        <Text style={{flex : 1,padding : 2, paddingTop : 11, fontSize : 12, textAlign : 'right'}}>{country}</Text>
                        <Image source={img_icon_earth} style={styles.icon}/>
                    </TouchableOpacity>
                </View>
                <Modal
                    isVisible={this.state.isCountryModalVisible}
                    onRequestClose={this._hideCountryModal}
                    onBackButtonPress={this._hideCountryModal}
                    onBackdropPress={this._hideCountryModal}>
                    <View style={{ flex: 1,flexDirection : 'column',justifyContent:'flex-end'}}>
                        <View style={{flex : 0.5, backgroundColor:'white'}}>
                            <ScrollView style={styles.scrollContainer}>
                                {countries.map((item,i) => {
                                    return(
                                        <TouchableOpacity
                                            key={i}
                                            onPress={()=>{this._countryChanged(item)}}>
                                            <Text style={styles.countryItem}>{item}</Text>
                                        </TouchableOpacity>
                                    )
                                })}
                            </ScrollView>
                        </View>
                    </View>
                </Modal>
                <Modal
                    isVisible={this.state.isCategoryModalVisible}
                    onRequestClose={this._hideCategoryModal}
                    onBackButtonPress={this._hideCategoryModal}
                    onBackdropPress={this._hideCategoryModal}>
                    <View style={{ flex: 1,flexDirection : 'column',justifyContent:'flex-end'}}>
                        <View style={{flex : 0.5, backgroundColor:'white'}}>
                            <ScrollView style={styles.scrollContainer}>
                                {categories.map((item,i) => {
                                    return(
                                        <TouchableOpacity
                                            key={i}
                                            onPress={()=>{this._categoryChanged(item)}}>
                                            <Text style={styles.categoryItem}>{item}</Text>
                                        </TouchableOpacity>
                                    )
                                })}
                            </ScrollView>
                        </View>
                    </View>
                </Modal>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container : {
        width : "100%",
        borderBottomColor : "#dddddd",
        borderBottomWidth : 5,
        justifyContent:'center',
        alignItems : 'center'
    },
    title : {
        color : color_blue_main,
        fontSize : 10,
        textAlign : 'center'
    },
    icon : {
        display : 'flex',
        width : 30,
        height : 30,
        margin : 5
    },
    scrollContainer : {

    },
    countryItem : {
        paddingLeft : 30,
        padding : 15,
        fontSize : 20
    },
    categoryItem : {
        paddingLeft : 30,
        padding : 15,
        fontSize : 20
    }
})
export default SelectCategories;