import React, { Component } from 'react';
import { Platform, View, Text,TextInput, Image, StyleSheet,ScrollView, TouchableOpacity } from 'react-native';
import Modal from 'react-native-modal';

import ImagePicker from 'react-native-image-picker';
import ImageResizer from 'react-native-image-resizer';

import BottomNavigationBar from '@components/bottomNavigationBar';

import TopBar from './topBar';

import {connect} from 'react-redux';

class EditProfileScreen extends Component{
    constructor(){
        super();
        this.state={
            myName : "",
            avatar : null,
            isModalVisible : false,
        }
    }

    componentDidMount(){
        const { profile } = this.props.myProfile;
        this.setState({
            myName : profile.name,
            avatar : profile.avatar
        })
    }

    _takeImage = (option) => {
        this._hideModal();
        const options = {
            quality: 1.0,
            storageOptions: {
              skipBackup: true,
              path: 'images',
            },
        };
        switch (option) {
            case 'camera':
                ImagePicker.launchCamera(options, (response) => {
                    this.onImagePicker(response, 'FromCamera');
                });
                break;
            case 'gallery':
                ImagePicker.launchImageLibrary(options, (response) => {
                    this.onImagePicker(response, 'FromLibrary');
                });
                break;
            default:
          }
    }

    onImagePicker = (response, whereFrom) => {
        if (response.didCancel) {
            alert('User cancelled image picker');
        } else if (response.error) {
            alert('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
            alert('User tapped custom button: ', response.customButton);
        } else if (response.uri !== undefined) {
            let source = '';

            if (Platform.OS === 'android') {
                source = { uri: response.uri };
            } else {
                source = { uri: response.uri.replace('file://', ''), isStatic: true };
            }

            var degree = 0;

            // this.setState({image : source.uri});
            ImageResizer.createResizedImage(source.uri, 200, 200, 'JPEG', 100, degree)
                .then((resizedImageUri) => {
                    this.setState({
                        avatar: resizedImageUri,
                    });
                }).catch((err) => {
                    alert(err);
                });
        }
    }

    _hideModal = () => {
        this.setState({isModalVisible : false})
    }

    _saveProfile = () => {
        alert('save profile');
    }

    render(){
        const { myName, avatar} = this.state;
        let avatarImage = avatar?(
            <TouchableOpacity onPress={()=>this.setState({isModalVisible:true})}>
                <Image source={avatar} style={styles.avatar}/>
            </TouchableOpacity>
        ):null;

        let name = myName?(
            <TextInput style={{fontSize : 23,borderBottomColor:'#d8d8d8',borderBottomWidth:2,flex : 1}} value={myName} onChangeText={text=>this.setState({myName : text})}/>
        ):null;
        
        return(
            <View style={styles.container}>
                <TopBar onBack={()=>this.props.navigation.goBack()} onSavePress={this._saveProfile}/>
                <View style={styles.content}>
                    <View style={styles.briefProfileInfo}>
                        {avatarImage}
                        <View style={{marginLeft : 15,marginTop : 7,flex : 1}}>
                            {name}
                        </View>
                    </View>
                </View>
                <BottomNavigationBar navigation={this.props.navigation} activeTab="setting"/>
                <Modal
                    isVisible={this.state.isModalVisible}
                    onRequestClose={this._hideModal}
                    onBackButtonPress={this._hideModal}
                    onBackdropPress={this._hideModal}>
                    <View style={{ flex: 1,flexDirection : 'column',justifyContent:'flex-end'}}>
                        <View style={{display : 'flex', backgroundColor:'white'}}>
                            <TouchableOpacity
                                onPress={()=>{this._takeImage('camera')}}>
                                <Text style={styles.modalItem}>From Camera</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={()=>{this._takeImage('gallery')}}>
                                <Text style={styles.modalItem}>From Gallery</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "white",
    },
    content : {
        flex : 1,
        width : "100%",
        backgroundColor : '#e8e8e8'
    },
    thumbContainer : {
        flexDirection : 'row',
        backgroundColor : 'white',
        padding : 5,
        borderBottomColor : '#e8e8e8',
        borderBottomWidth : 2
    },
    briefProfileInfo : {
        flexDirection : 'row',
        backgroundColor : 'white',
        borderBottomColor : "#e8e8e8",
        borderBottomWidth : 4,
        padding : 20
    },
    avatar : {
        display : 'flex',
        width : 70,
        height : 70,
        borderRadius : 35,
        borderColor : '#e8e8e8',
        borderWidth : 2
    },
    navigationList : {
        flex : 1,
        padding : 20,
        paddingLeft : 30,
        backgroundColor : 'white'
    },
    navigationItem : {
        paddingTop : 15,
        paddingBottom : 15,
        fontSize : 18
    },
    modalItem : {
        paddingLeft : 30,
        padding : 15,
        fontSize : 20
    }
});

function mapStateToProps(state) {
    return {
        myProfile : state.myProfile
    }
}
export default connect(mapStateToProps)(EditProfileScreen);