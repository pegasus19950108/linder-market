import React, {Component} from 'react';
import {Platform, StyleSheet, View, Image, StatusBar} from 'react-native';

import { color_blue_main } from '@themes/color';
import { img_logo } from '@constants'

export default class App extends Component {
    componentDidMount(){
        StatusBar.setHidden(true);
        setTimeout(() => {
            this.props.navigation.navigate('loginScreen');
        }, 1000);
    }
    render() {
        return (
        <View style={styles.container}>
            <Image
                source={img_logo}
                style = {styles.imgLogo}
                ></Image>
        </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: color_blue_main,
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    imgLogo : {
        width : 200,
        height : 200
    }
});