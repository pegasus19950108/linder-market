import React, {Component} from 'react';
import {Platform, StyleSheet, View, StatusBar,Text, TouchableOpacity, ScrollView} from 'react-native';

import BottomNavigationBar from '@components/bottomNavigationBar';
import ButtonAddCircle from '@components/buttonAddCircle';

import SearchBar from './searchBar';
import NavBar from './navBar';

import Thumb from './thumb';

import { connect } from 'react-redux';

import { color_blue_main } from '@themes/color';

class DepartmentScreen extends Component {
    constructor(props){
        super(props);
        this.state = {
            email : ""
        }
    }
    componentDidMount(){
        StatusBar.setHidden(true);
    }
    _onLayoutDidChange = (e) => {
        const layout = e.nativeEvent.layout;
        this.setState({ carouselSize: { width: layout.width, height: 300 } });
    }
    thumbPressed = (data) =>{
        this.props.navigation.navigate('subDepartmentScreen');
    }
    render() {
        const { thumbs, departments, currentDepartment } = this.props.departmentData;
        return (
            <View style={styles.container}>
                <SearchBar 
                    onBack={()=>{
                        this.props.navigation.goBack();
                    }}
                    onSearchPress={()=>{
                        this.props.navigation.navigate('searchScreen');
                    }}
                    />
                <NavBar data={departments} current={currentDepartment}/>
                <View style={styles.content}>
                    <ScrollView style={styles.scrollContainer}>
                        <View style={styles.thumbContainer}>
                            {
                                thumbs.map((item,i) =>{
                                    return(
                                        <Thumb key={i} data={item} onPress={this.thumbPressed}/>
                                    )
                                })
                            }
                        </View>
                        <ButtonAddCircle onAddPress={()=>{this.props.navigation.navigate('uploadNewsScreen')}}/>
                    </ScrollView>
                </View>
                <BottomNavigationBar navigation={this.props.navigation} activeTab="home"/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "white",
    },
    content : {
        flex : 1
    },
    scrollContainer : {
        position : 'relative',
        backgroundColor : '#e8e8e8'
    },
    thumbContainer : {
        flex : 1,
        flexDirection : 'row',
        flexWrap : 'wrap',
    },
});

function mapStateToProps(state) {
    return {
        departmentData : state.departmentData
    }
}
export default connect(mapStateToProps)(DepartmentScreen);