import React, { Component } from 'react';
import { View, Image, Text, StyleSheet, TouchableOpacity } from 'react-native';

class Thumb extends Component{
    constructor(props){
        super(props);
    }

    render(){
        const {title,thumbImage} = this.props.data;

        return(
            <View style={styles.container}>
                <TouchableOpacity onPress={()=>{
                    this.props.onPress(this.props.data)
                    }}>
                    <Image style={styles.thumbImage} source={thumbImage}/>
                </TouchableOpacity>
                <Image style={styles.gradient} source={require('@assets/images/gradient-background.png')}/>
                <Text style={styles.title}>{title}</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container : {
        width : "50%",
        position : 'relative',
        borderColor : "white",
        borderWidth : 4
    },
    thumbImage : {
        width : "100%",
        height : 150
    },
    gradient : {
        position : 'absolute',
        width : '100%',
        height : 30,
        bottom : 0
    },
    title : {
        position : 'absolute',
        bottom : 0,
        color : 'white',
        padding : 10,
        fontSize : 12,
        paddingBottom : 7
    }
})

export default Thumb;