import React, { Component } from 'react'
import { View, TextInput, Text, Image, StyleSheet, ScrollView, TouchableOpacity } from 'react-native';
import { color_blue_main } from '@themes/color';

import {
    Menu,
    MenuOptions,
    MenuOption,
    MenuTrigger,
  } from 'react-native-popup-menu';

import {
    img_arrow_back,
    img_icon_search,
    img_icon_earth,
    img_icon_menu,
    img_icon_star
} from '@constants';

class SearchBar extends Component{
    constructor(){
        super();
        this.state ={
            isModalVisible : false,
        }
    }

    render(){
        const section = this.props.title;
        const currentCountry = this.state.currentCountry;
        return(
            <View style={styles.container}>
                <Text style={styles.title}>Linder APAC Intelligence Portal</Text>
                <View style={{flexDirection:'row',width:"95%",padding:10}}>
                    <TouchableOpacity onPress={()=>{this.props.onBack()}}>
                        <Image source={img_arrow_back} style={styles.icon}/>
                    </TouchableOpacity>
                    <Text style={styles.section}>{section}</Text>
                    <TouchableOpacity
                        style={{flexDirection:'row'}}
                        >
                        <Image source={img_icon_star} style={styles.icon}/>
                    </TouchableOpacity>
                    
                    <Menu>
                        <MenuTrigger>
                            <Image style={styles.icon} source={img_icon_menu}/>
                        </MenuTrigger>
                        <MenuOptions>
                            <MenuOption onSelect={() => alert('download')} >
                                <Text style={styles.menuItem}>Download as PDF</Text>
                            </MenuOption>
                            <MenuOption onSelect={() => alert(`Delete`)} >
                                <Text style={styles.menuItem}>Share</Text>
                            </MenuOption>
                        </MenuOptions>
                    </Menu>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container : {
        width : "100%",
        borderBottomColor : "#dddddd",
        borderBottomWidth : 5,
        justifyContent:'center',
        alignItems : 'center'
    },
    title : {
        color : color_blue_main,
        fontSize : 10,
        textAlign : 'center'
    },
    icon : {
        display : 'flex',
        width : 30,
        height : 30,
        margin : 5,
        marginTop : 2
    },
    section:{
        flex : 1,
        padding : 5,
        fontSize : 18
    },
    scrollContainer : {
        // padding : 20
    },
    countryItem : {
        paddingLeft : 30,
        padding : 15,
        fontSize : 20
    },
    menuItem : {
        padding : 5,
        paddingLeft : 10,
        fontSize : 18
    }
})
export default SearchBar;