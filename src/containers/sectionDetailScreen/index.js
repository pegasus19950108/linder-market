import React, {Component} from 'react';
import {Platform, StyleSheet, View, StatusBar,Text, Image, TouchableOpacity, ScrollView} from 'react-native';

import BottomNavigationBar from '@components/bottomNavigationBar';
import ButtonAddCircle from '@components/buttonAddCircle';

import SearchBar from './searchBar';
import { connect } from 'react-redux';

import { color_blue_main } from '@themes/color';

import { MenuProvider } from 'react-native-popup-menu';
import {
    img_icon_like,
    img_icon_unlike
} from '@constants';

class SectionDetailScreen extends Component {
    constructor(props){
        super(props);
        
    }
    componentDidMount(){
        StatusBar.setHidden(true);
    }
    _onFollowUp = () => {
        this.props.navigation.navigate('sendFollowUp');
    }

    render() {
        const { 
            header, 
            title, 
            date, 
            thumbImage,
            highlight,
            content,
            tags,
            like,
            unlike
        } = this.props.sectionDetailData;
        return (
        <MenuProvider
            skipInstanceCheck={true}>
            <View style={styles.container}>
                <SearchBar onBack={()=>{this.props.navigation.goBack()}}/>
                <View style={styles.content}>
                    <ScrollView style={styles.scrollContainer}>
                        <Text style={styles.header}>{header}</Text>
                        <Text style={styles.title}>{title}</Text>
                        <Text style={styles.date}>{date}</Text>
                        <Image style={styles.thumbImage} source={thumbImage}/>
                        <Text style={styles.highlight}>{highlight}</Text>
                        {content.map((item,i)=>{
                            return(
                                <Text key={i} style={styles.textContent}>{item}</Text>
                            )
                        })}
                        <View style={styles.tagContainer}>
                            {tags.map((item, i) => {
                                return(
                                    <Text key={i} style={styles.tag}>{item}</Text>
                                )
                            })}
                        </View>
                        <View style={{flex : 1,width : "100%",alignItems : 'center'}}>
                            <TouchableOpacity style={styles.btnFollowUp} onPress={this._onFollowUp}>
                                <View style={{flexDirection:'row'}}>
                                    <Text style={{display : 'flex',color:color_blue_main}}>@  </Text>
                                    <Text style={{display : 'flex'}}>FOLLOW UP</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={{flexDirection:'row',width : "100%",alignItems : 'center',justifyContent:'center'}}>
                            <View style={{width : '80%',flexDirection:'row'}}>
                                <View style={{flex : 0.5}}>
                                    <TouchableOpacity style={styles.btnLike}>
                                        <View style={{flexDirection:'row'}}>
                                            <Image source={img_icon_like} style={styles.btnImage}/>
                                            <Text style={{display : 'flex',color:'white'}}>{like?like:0}</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                                <View style={{flex : 0.5}}>
                                    <TouchableOpacity style={styles.btnUnlike}>
                                        <View style={{flexDirection:'row'}}>
                                            <Image source={img_icon_unlike} style={styles.btnImage}/>
                                            <Text style={{display : 'flex'}}>{unlike?unlike:0}</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                        <ButtonAddCircle onAddPress={()=>{this.props.navigation.navigate('uploadNewsScreen')}}/>
                    </ScrollView>
                </View>
                <BottomNavigationBar navigation={this.props.navigation} activeTab="home"/>
            </View>
        </MenuProvider>);
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "white",
    },
    content : {
        flex : 1,
        width : "100%"
    },
    scrollContainer : {
        width : '100%'
    },
        header : {
            color : color_blue_main,
            paddingTop : 20,
            paddingLeft : 20,
            paddingBottom : 7
        },
        title : {
            width : "100%",
            fontSize : 24,
            paddingTop : 7,
            paddingLeft : 20,
            paddingRight : 20,
            paddingBottom : 10
        },
        date : {
            color : color_blue_main,
            paddingTop : 10,
            paddingLeft : 20,
            paddingBottom : 7
        },
        thumbImage : {
            width : "100%",
            height : 180
        },
        highlight : {
            color : "#7eb0e5",
            paddingTop : 4,
            paddingLeft : 20,
            paddingBottom : 7
        },
        textContent : {
            lineHeight : 27,
            fontSize : 16,
            paddingTop : 10,
            paddingLeft : 20,
            paddingRight : 20,
            paddingBottom : 10
        },
        tagContainer : {
            padding : 20,
            flex : 1,
            flexDirection : 'row'
        },
            tag : {
                display : 'flex',
                color : color_blue_main,
                backgroundColor : "#e7effc",
                paddingLeft : 5,
                paddingRight : 5,
                margin : 5
            },
        btnFollowUp : {
            justifyContent: 'center',
            alignItems: 'center',
            height : 50,
            width : "80%",
            borderColor : color_blue_main,
            borderWidth : 1,
            backgroundColor : "white",
            borderRadius : 30,
            margin : 20,
        },
        btnLike : {
            justifyContent: 'center',
            alignItems: 'center',
            height : 50,
            backgroundColor : color_blue_main,
            borderColor : color_blue_main,
            borderWidth : 1,
            borderRadius : 30,
            marginRight : 10,
        },
        btnUnlike : {
            justifyContent: 'center',
            alignItems: 'center',
            height : 50,
            borderColor : color_blue_main,
            borderWidth : 1,
            backgroundColor : "white",
            borderRadius : 30,
            marginLeft : 10,
        },
        btnImage:{
            width : 20,
            height : 20,
            marginRight : 10
        }
});

function mapStateToProps(state) {
    return {
        sectionDetailData : state.sectionDetailData
    }
}
export default connect(mapStateToProps)(SectionDetailScreen);