import React, { Component } from 'react';
import { View, Text,TextInput, Image, StyleSheet,ScrollView, TouchableOpacity } from 'react-native';
import {MenuProvider,MenuContext} from 'react-native-popup-menu';

import BottomNavigationBar from '@components/bottomNavigationBar';

import TopBar from './topBar';
// import Thumb from './thumb';

import {connect} from 'react-redux';

import { color_blue_main } from '@themes/color';

import {
    img_icon_add,
    img_icon_close
} from '@constants';

class MySettingScreen extends Component{
    constructor(){
        super();
        this.state={
            currentEmail : "",
            emails : [],
            autoCompleteMenuVisible : false,
        }
    }
    render(){
        const { profile } = this.props.myProfile;

        let avatarImage = profile.avatar?(
            <Image source={profile.avatar} style={styles.avatar}/>
        ):null;

        let name = profile.name?(
            <Text style={{fontSize : 23}}>{profile.name}</Text>
        ):null;

        let email = profile.email?(
            <Text style={{color : "#a9a9a9",marginTop : 5}}>{profile.email}</Text>
        ):null;
        
        return(
            <MenuProvider
                skipInstanceCheck={true}>
                <View style={styles.container}>
                    <TopBar/>
                    <View style={styles.content}>
                        <View style={styles.briefProfileInfo}>
                            {avatarImage}
                            <View style={{marginLeft : 15,marginTop : 7}}>
                                {name}
                                {email}
                            </View>
                        </View>
                        <View style={styles.navigationList}>
                            <TouchableOpacity
                                onPress={()=>this.props.navigation.navigate('editProfileScreen')}>
                                <Text style={styles.navigationItem}>Edit Profile</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={()=>this.props.navigation.navigate('editTagScreen')}>
                                <Text style={styles.navigationItem}>Edit Tag</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={()=>this.props.navigation.navigate('changePasswordScreen')}>
                                <Text style={styles.navigationItem}>Change Password</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={()=>this.props.navigation.navigate('termConditionScreen')}>
                                <Text style={styles.navigationItem}>Terms & Condition</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={()=>this.props.navigation.navigate('privacyPolicyScreen')}>
                                <Text style={styles.navigationItem}>Privacy Policy</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={()=>this.props.navigation.navigate('profileSettingScreen')}>
                                <Text style={styles.navigationItem}>Setting</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <BottomNavigationBar navigation={this.props.navigation} activeTab="setting"/>
                </View>
            </MenuProvider>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "white",
    },
    content : {
        flex : 1,
        width : "100%",
        backgroundColor : '#e8e8e8'
    },
    thumbContainer : {
        flexDirection : 'row',
        backgroundColor : 'white',
        padding : 5,
        borderBottomColor : '#e8e8e8',
        borderBottomWidth : 2
    },
    briefProfileInfo : {
        flexDirection : 'row',
        backgroundColor : 'white',
        borderBottomColor : "#e8e8e8",
        borderBottomWidth : 4,
        padding : 20
    },
    avatar : {
        display : 'flex',
        width : 70,
        height : 70,
        borderRadius : 35,
        borderColor : '#e8e8e8',
        borderWidth : 2
    },
    navigationList : {
        flex : 1,
        padding : 20,
        paddingLeft : 30,
        backgroundColor : 'white'
    },
    navigationItem : {
        paddingTop : 15,
        paddingBottom : 15,
        fontSize : 18
    }
});

function mapStateToProps(state) {
    return {
        myProfile : state.myProfile
    }
}
export default connect(mapStateToProps)(MySettingScreen);