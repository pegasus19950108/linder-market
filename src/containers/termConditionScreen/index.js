import React, { Component } from 'react';
import { Platform, View, Text,TextInput, Image, StyleSheet,ScrollView, TouchableOpacity } from 'react-native';
import {connect} from 'react-redux';

import BottomNavigationBar from '@components/bottomNavigationBar';

import TopBar from './topBar';

import { color_blue_main } from '@themes/color';

class TermConditionScreen extends Component{
    constructor(){
        super();
        this.state={};
    }

    _saveProfile = () => {
        alert('save profile');
    }

    render(){
        const { termConditionData } = this.props.myProfile;
        return(
            <View style={styles.container}>
                <TopBar onBack={()=>this.props.navigation.goBack()}/>
                <View style={styles.content}>
                    <ScrollView style={styles.sectionContainer}>
                        {
                            termConditionData.map((item,i)=>{
                                return(
                                    <Text key={i} style={styles.section}>{item}</Text>
                                )
                            })
                        }
                    </ScrollView>
                </View>
                <BottomNavigationBar navigation={this.props.navigation} activeTab="setting"/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "white",
    },
    content : {
        flex : 1,
        width : "100%",
    },
    sectionContainer : {
        padding : 20,
    },
    section : {
        fontSize : 15,
        lineHeight : 24,
        marginBottom : 20
    }
});

function mapStateToProps(state) {
    return {
        myProfile : state.myProfile
    }
}
export default connect(mapStateToProps)(TermConditionScreen);