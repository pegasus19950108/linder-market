import React, { Component } from 'react'
import { View, TextInput, Text, Image, StyleSheet, TouchableOpacity } from 'react-native';
import { color_blue_main } from '@themes/color';

import {
    img_icon_close,
    img_icon_search
} from '@constants';

class SearchBar extends Component{
    render(){
        return(
            <View style={styles.container}>
                <Text style={styles.title}>Linder APAC Intelligence Portal</Text>
                <View style={{flexDirection:'row',width:"95%",padding:10}}>
                    <TouchableOpacity>
                        <Image source={img_icon_close} style={styles.icon}/>
                    </TouchableOpacity>
                    <TextInput style={styles.input} placeholder="Enter keywords, #tags..."/>
                    <TouchableOpacity
                        onPress={this.props.onSearchPress}>
                        <Image source={img_icon_search} style={styles.icon}/>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container : {
        width : "100%",
        borderBottomColor : "#dddddd",
        borderBottomWidth : 5,
        justifyContent:'center',
        alignItems : 'center'
    },
    title : {
        color : color_blue_main,
        fontSize : 10,
        textAlign : 'center'
    },
    icon : {
        display : 'flex',
        width : 30,
        height : 30,
        margin : 5
    },
    input:{
        flex : 1,
        padding : 5,
        fontSize : 16
    }
})
export default SearchBar;