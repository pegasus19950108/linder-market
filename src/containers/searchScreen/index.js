import React, {Component} from 'react';
import {Platform, StyleSheet, View, StatusBar,Text, TouchableOpacity, ScrollView} from 'react-native';
import {MenuProvider,MenuContext} from 'react-native-popup-menu';

import BottomNavigationBar from '@components/bottomNavigationBar';
import ButtonAddCircle from '@components/buttonAddCircle';

import SearchBar from './searchBar';
import SelectCategories from './selectCategories';

import Thumb from './thumb';

import { connect } from 'react-redux';

import { color_blue_main } from '@themes/color';

class SearchScreen extends Component {
    constructor(props){
        super(props);
        this.state = {
            email : ""
        }
    }
    componentDidMount(){
        StatusBar.setHidden(true);
    }
    _onLayoutDidChange = (e) => {
        const layout = e.nativeEvent.layout;
        this.setState({ carouselSize: { width: layout.width, height: 300 } });
      }

    thumbPressed = (data) =>{
        // this.props.navigation.navigate('departmentScreen');
    }
    render() {
        const { data } = this.props.searchResult;
        return (
            <MenuProvider>
                <View style={styles.container}>
                <SearchBar/>
                <SelectCategories/>
                <View style={styles.content} onLayout={this._onLayoutDidChange}>
                    <ScrollView style={styles.scrollContainer}>
                        <View style={styles.thumbContainer}>
                            {
                                data.map((item,i) =>{
                                    return(
                                        <Thumb key={i} data={item} onPress={this.thumbPressed}/>
                                    )
                                })
                            }
                        </View>
                        <ButtonAddCircle onAddPress={()=>{this.props.navigation.navigate('uploadNewsScreen')}}/>
                    </ScrollView>
                </View>
                <BottomNavigationBar navigation={this.props.navigation} activeTab="home"/>
            </View>
            </MenuProvider>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "white",
    },
    scrollContainer : {
        position : 'relative',
        backgroundColor : '#e8e8e8'
    },
    content : {
        flex : 1,
        width : "100%",
        backgroundColor : '#e8e8e8'
    },
    thumbContainer : {
        width : "100%"
    }
});

function mapStateToProps(state) {
    return {
        searchResult : state.searchResult
    }
}
export default connect(mapStateToProps)(SearchScreen);