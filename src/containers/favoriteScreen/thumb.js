import React, { Component } from 'react';
import { View, Image, Text, StyleSheet, TouchableOpacity } from 'react-native';
import {
    Menu,
    MenuOptions,
    MenuOption,
    MenuTrigger,
    renderers
  } from 'react-native-popup-menu';
import { color_blue_main } from '@themes/color';

import {
    img_icon_star_filled,
    img_icon_menu
} from '@constants';

const { SlideInMenu,Popover,ContextMenu } = renderers;

class Thumb extends Component{
    constructor(props){
        super(props);
    }

    render(){
        const {thumbImage,country,content,tags} = this.props.data;

        return(
            <View style={styles.container}>
                <TouchableOpacity onPress={()=>{
                    // this.props.onPress(this.props.data)
                    }}>
                    <Image style={styles.thumbImage} source={thumbImage}/>
                </TouchableOpacity>
                <View style={{flexDirection : 'column',flex : 1,padding : 5}}>
                    <View style={styles.country}>
                        <Text style={styles.countryText}>{country}</Text>
                        <TouchableOpacity>
                            <Image style={styles.icon} source={img_icon_star_filled}/>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <Menu renderer={ContextMenu}>
                                <MenuTrigger>
                                    <Image style={styles.icon} source={img_icon_menu}/>
                                </MenuTrigger>
                                <MenuOptions>
                                    <MenuOption onSelect={() => alert(country)} >
                                        <Text style={styles.menuItem}>Download as PDF</Text>
                                    </MenuOption>
                                    <MenuOption onSelect={() => alert(`Delete`)} >
                                        <Text style={styles.menuItem}>Share</Text>
                                    </MenuOption>
                                </MenuOptions>
                            </Menu>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.content}>
                        <Text style={styles.contentText}>{content}</Text>
                    </View>
                    <View style={styles.tags}>
                        {tags.map((item,i)=>{
                            return(
                                <Text key={i} style={styles.tag}>{item}</Text>
                            )
                        })}
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container : {
        flexDirection : 'row',
        backgroundColor : 'white',
        padding : 5,
        borderBottomColor : '#e8e8e8',
        borderBottomWidth : 2
    },
    thumbImage : {
        width : 130,
        height : 130
        // height : 150
    },
    country : {
        display : 'flex',
        justifyContent:'flex-start',
        flexDirection : 'row'
    },
        countryText : {
            color : color_blue_main,
            flex : 1,
            fontSize : 14,
            marginTop : 5
        },
        icon : {
            display : 'flex',
            width : 27,
            height : 27,
            margin : 4,
            marginTop : 2,
            marginBottom : 2
        },
    content : {
        display : 'flex',
        flex : 1,
        width : "100%"
    },
        contentText : {
            fontSize : 16
        },
    tags : {
        display : 'flex',
        flexDirection : 'row'
    },
        tag : {
            display : 'flex',
            color : color_blue_main,
            backgroundColor : '#e7effc',
            margin : 4,
            paddingLeft : 4,
            paddingRight : 4
        },
    menuItem : {
        padding : 5,
        paddingLeft : 10,
        fontSize : 18
    }
})

export default Thumb;