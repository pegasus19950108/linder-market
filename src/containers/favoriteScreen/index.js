import React, { Component } from 'react';
import { View, Text,TextInput, Image, StyleSheet,ScrollView, TouchableOpacity } from 'react-native';
import {MenuProvider,MenuContext} from 'react-native-popup-menu';

import BottomNavigationBar from '@components/bottomNavigationBar';

import TopBar from './topBar';
import Thumb from './thumb';

import {connect} from 'react-redux';

import { color_blue_main } from '@themes/color';

import {
    img_icon_add,
    img_icon_close
} from '@constants';

class FavoriteScreen extends Component{
    constructor(){
        super();
        this.state={
            currentEmail : "",
            emails : [],
            autoCompleteMenuVisible : false,
        }
    }
    render(){
        const { data } = this.props.favoriteData;

        return(
            <MenuProvider
                skipInstanceCheck={true}>
                <View style={styles.container}>
                    <TopBar/>
                    <View style={styles.content}>
                        <ScrollView>
                            {
                                data.map((item,i) =>{
                                    return(
                                        <Thumb key={i} data={item} onPress={this.thumbPressed}/>
                                    )
                                })
                            }
                        </ScrollView>
                    </View>
                    <BottomNavigationBar navigation={this.props.navigation} activeTab="favorite"/>
                </View>
            </MenuProvider>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "white",
    },
    content : {
        flex : 1,
        width : "100%",
        backgroundColor : '#e8e8e8'
    },
    thumbContainer : {
        flexDirection : 'row',
        backgroundColor : 'white',
        padding : 5,
        borderBottomColor : '#e8e8e8',
        borderBottomWidth : 2
    },
});

function mapStateToProps(state) {
    return {
        favoriteData : state.favoriteData
    }
}
export default connect(mapStateToProps)(FavoriteScreen);