import React, { Component } from 'react';
import { Platform, View, Text,TextInput, Image, StyleSheet,ScrollView, TouchableOpacity } from 'react-native';
import {connect} from 'react-redux';

import BottomNavigationBar from '@components/bottomNavigationBar';

import TopBar from './topBar';

import { color_blue_main } from '@themes/color';
import {
    img_icon_add,
    img_icon_close
} from '@constants';

class EditTagScreen extends Component{
    constructor(){
        super();
        this.state={
            currentTag : "",
            tags : ["tag 1","tag 2"]
        };
    }

    componentWillMount(){
        let tags = this.props.myProfile.tags;
        this.setState({tags : tags});
    }
    _addTag = () => {
        let tags = this.state.tags;
        if(this.state.currentTag)
        {
            tags.push(this.state.currentTag);
            this.setState({tags : tags});
        }
    }
    
    _removeTag = (i) => {
        let tags = this.state.tags;
        tags.splice(i,1);
        this.setState({tags : tags});
    }

    _saveTags = () => {
        alert('save tags')
    }

    render(){
        const { currentTag,tags } = this.state;
        let tagSection;
        if(tags && tags.length)
            tagSection = (
                <View style={styles.tagContainer}>
                    {
                        tags.map((item, i)=>{
                            return(
                                <View key={i} style={styles.tag}>
                                    <Text style={styles.tagText}>{item}</Text>
                                    <TouchableOpacity onPress={()=>this._removeTag(i)}>
                                        <Image source={img_icon_close} style={styles.tagClose}/>
                                    </TouchableOpacity>
                                </View>
                            )
                        })
                    }
                </View>
            );
        else
            tagSection = (
                <Text style={{width:"100%",textAlign:'center',fontSize:20,color :'#d8d8d8',paddingTop : 30}}>Add your favorite tag to receive notification!</Text>
            )

        return(
            <View style={styles.container}>
                <TopBar onBack={()=>this.props.navigation.goBack()} onSavePress={this._saveTags}/>
                <View style={styles.content}>
                    <View style={styles.inputContainer}>
                        <TextInput style={{flex : 1}} placeholder="Add tag..." value={currentTag} onChangeText={text => this.setState({currentTag : text})}/>
                        <TouchableOpacity onPress={this._addTag}>
                            <Image source={img_icon_add} style={styles.icon}/>
                        </TouchableOpacity>
                    </View>
                    <ScrollView style={styles.sectionContainer}>
                        {tagSection}
                    </ScrollView>
                </View>
                <BottomNavigationBar navigation={this.props.navigation} activeTab="setting"/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "white",
    },
    content : {
        flex : 1,
        width : "100%",
    },
    inputContainer : {
        width : "90%", 
        flexDirection : 'row', 
        borderBottomColor : '#a9a9a9',
        borderBottomWidth : 1,
        marginTop : 20,
        marginBottom : 10,
        marginLeft : "5%"
    },
    icon : {
        width : 30,
        height : 30,
        marginTop : 12
    },
    sectionContainer : {
        padding : "5%",
        paddingTop : 10,
    },
    tagContainer : {
        width : "100%",
        alignItems : 'flex-start',
        textAlign : 'left',
        flexDirection : 'row',
        flexWrap : 'wrap',
        flex : 1
    },
    tag : {
        justifyContent: 'center',
        alignItems: 'center',
        height : 40,
        backgroundColor : 'white',
        borderColor : color_blue_main,
        borderWidth : 1,
        borderRadius : 30,
        margin : 5,
        paddingLeft : 15,
        paddingRight : 8,
        flexDirection : 'row',
    },
    tagText : {
        display : 'flex',
        color : color_blue_main,
    },
    tagClose : {
        display : 'flex',
        width : 30,
        height : 30,
    }
});

function mapStateToProps(state) {
    return {
        myProfile : state.myProfile
    }
}
export default connect(mapStateToProps)(EditTagScreen);