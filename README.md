# linder-market
React native project for linder market mobile app.

##environment
###Android
Android SDK, Android Studio, JDK

####Environment Setup
System Environment setup

Add JAVA_HOME and ANDROID_HOME

ADD %ANDROID_HOME%/platform-tools to path variable

###iOS
macOS 10.13, xcode 10

##installation
Install react-native-cli
```
npm i -g react-native-cli
```
##running this project
clone this project
```
cd linder-market
```
```
npm install
```

###Android
```
react-native run-android
```
###iOS

open iGear.xcodeproj
and click run button

alternatively
```
react-native run-ios --simulator="iphone 7"
```